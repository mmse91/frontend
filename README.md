# Frontend der THM Feedback App

Dieses Projekt beinhaltet das Frontend für die THM Feedback App, welche im Rahmen des Softwaretechnik-Projekts von Studierenden der THM im Sommersemester 2020 entwickelt wurde.


Am Frontend beteiligt waren:

Britta Fichtl, Pia-Doreen Ritzke, Marvin Matteo Seck-Eichhorn und Daniela Claudia Coenen (Projektleitung).

## Dokumentation

Eine umfangreiche Dokumentation in Hinblick auf die Bedienung und die Entwicklung der Anwendung ist der [Wiki](https://git.thm.de/swtp-feedback-app/frontend/-/wikis/home) dieses Projektes zu entnehmen.

Informationen rund um das Backend dieser Anwendung sind in einem extra [Repository](https://git.thm.de/swtp-feedback-app/backend) und dessen [Wiki](https://git.thm.de/swtp-feedback-app/backend/-/wikis/home) zu finden.
