import 'interfaces/i_api.dart';

import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/interfaces/i_api.dart';

class QuizService {
  QuizService({IApi api}) : _api = api;
  IApi _api;
  Quiz _fetchedQuiz;
  List<Session> _fetchedSessions;

  Quiz get quiz => _fetchedQuiz;

  List<Session> get sessions => _fetchedSessions;

  ///
  /// Get all sessions assigned to this quiz.
  Future<void> getSessionsForQuiz(Token authToken, Quiz quiz) async {
    _fetchedQuiz = quiz;
    try {
      _fetchedSessions = await _api.getSessionsForQuiz(quiz, authToken);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Adds a new session to a quiz. If a course is given, assigns the session to the course.
  ///
  Future<void> submitSession(Quiz q, Token authToken, Session s,
      [Course course]) async {
    _fetchedQuiz = q;
    try {
      Session returnSession = await _api.startSession(q, s, authToken);
      if (course != null) {
        submitSessionToCourse(course, returnSession.id, authToken);
      }
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Assigns a session to a course.
  Future<void> submitSessionToCourse(
      Course course, int sessionId, Token authToken) async {
    try {
      await _api.submitCourseSession(course, sessionId, authToken);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Gets the questions to a given quiz.
  Future<void> getQuestions(Quiz q, Token authToken) async {
    _fetchedQuiz = q;
    try {
      _fetchedQuiz.questions =
          await _api.getQuestions(_fetchedQuiz.id, authToken);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Deletes the given quiz.
  void deleteQuiz(Quiz quiz, Token authToken) async {
    try {
      _api.deleteQuiz(quiz, authToken);
    } catch (e) {
      throw e;
    }
  }
}
