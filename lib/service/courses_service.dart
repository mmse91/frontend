import 'package:fbs_app/domain/entities/quiz.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/domain/entities/user.dart';

import '../domain/entities/course.dart';
import '../domain/entities/token.dart';
import 'common/input_parser.dart';
import 'interfaces/i_api.dart';

class CoursesService {
  CoursesService({IApi api}) : _api = api;
  IApi _api;
  List<Course> _initCourses = [];
  List<Course> _courses = [];
  List<Course> _initAllCourses = [];
  List<Course> _allCourses = [];
  List<Course> _initAdminCourses = [];
  List<Course> _adminCourses = [];
  List<Session> _undoneSessions = [];
  List<Session> _sessions = [];
  List<User> _admins = [];
  List<User> _participants = [];
  List<Quiz> _initQuizzes = [];
  List<Quiz> _quizzes = [];
  List<Session> _initSessionsForUser = [];
  List<Session> _sessionsForUser = [];
  List<Session> _sessionsForQuiz = [];
  List<User> _initStudents = [];
  List<User> _students = [];

  List<Course> get courses => _courses;

  List<Course> get allCourses => _allCourses;

  List<Course> get adminCourses => _adminCourses;

  List<Session> get undoneSessions => _undoneSessions;

  List<Session> get sessions => _sessions;

  List<User> get admins => _admins;

  List<User> get participants => _participants;

  List<Quiz> get quizzes => _quizzes;

  List<Session> get sessionsForUser => _sessionsForUser;

  List<Session> get sessionsForQuiz => _sessionsForQuiz;

  List<User> get students => _students;

  bool search = true;
  bool options = false;
  bool isAdmin = false;
  bool isOwner = false;

  Future<Course> joinCourse(
      int courseId, String password, Token authToken) async {
    try {
      Course course = await _api.joinCourse(courseId, authToken,
          password == "" ? null : InputParser.coursePassword(password));
      return course;
    } catch (e) {
      throw e;
    }
  }

  Future<void> addCourseAdmin(int courseId, User user, Token authToken) async {
    try {
      user = await _api.submitCourseAdmin(courseId, user, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> deleteCourseAdmin(
      int courseId, User user, Token authToken) async {
    try {
      await _api.deleteCourseAdmin(courseId, user, authToken);
    } catch (e) {
      throw e;
    }
  }

  void leaveCourse(int courseId, Token authToken) async {
    try {
      _api.leaveCourse(courseId, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCoursesForUser(Token token) async {
    try {
      _initCourses = await _api.getCoursesForUser(token, role: "participant");
      _courses = _initCourses;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getAdminCoursesForUser(Token token) async {
    _initCourses = await _api.getCoursesForUser(token, role: "adminOrOwner");
    _courses = _initCourses;
  }

  Future<void> getAllCourses(Token token) async {
    try {
      _initAllCourses = await _api.getAllCourses(token, true);
      _allCourses = _initAllCourses;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCoursesForUserFilter(String value) async {
    try {
      _courses = _initCourses
          .where(
              (item) => item.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getAllCoursesForUserFilter(String value) async {
    try {
      _allCourses = _initAllCourses
          .where((item) =>
              item.name.toLowerCase().contains(value.toLowerCase()) ||
              item.docent.lastname.toLowerCase().contains(value.toLowerCase()))
          .toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCoursesForAdmins(Token token) async {
    try {
      var _tmpOwnerOrAdminCourses =
          await _api.getCoursesForUser(token, role: "adminOrOwner");
      _initAdminCourses = _tmpOwnerOrAdminCourses;
      _adminCourses = _tmpOwnerOrAdminCourses;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCoursesForAdminsFilter(String value) async {
    try {
      _adminCourses = _initAdminCourses
          .where(
              (item) => item.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionsForCourse(int courseId, Token token) async {
    try {
      _sessions = await _api.getSessionsForCourse(courseId, token);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCourseAdmins(int courseId, Token authToken) async {
    try {
      _admins = await _api.getCourseAdmins(courseId, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> addCourse(Course course, Token authToken) async {
    try {
      await _api.submitCourse(course, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCourseParticipants(int courseId, Token authToken,
      {bool hideAdmins = false}) async {
    try {
      _participants = await _api.getCourseParticipants(courseId, authToken,
          hideAdmins: hideAdmins);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCourseStudents(int courseId, Token authToken) async {
    try {
      _initStudents = await _api.getCourseParticipants(courseId, authToken);
      _students = _initStudents;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getCourseStudentsFilter(String value) async {
    try {
      _students = _initStudents.where((item) {
        return item.lastname.toLowerCase().contains(value.toLowerCase()) ||
            item.firstname.toLowerCase().contains(value.toLowerCase()) ||
            item.username.toLowerCase().contains(value.toLowerCase());
      }).toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getUndoneSessionsForUser(Token authToken) async {
    try {
      _undoneSessions = await _api.getUndoneSessionsForUser(authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getQuizzesForUser(Token authToken) async {
    try {
      _initQuizzes = await _api.getQuizzesForUser(authToken);
      _quizzes = _initQuizzes;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getQuizzesForUserFilter(String value) async {
    try {
      _quizzes = _initQuizzes
          .where(
              (item) => item.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionsForUser(Token authToken) async {
    try {
      _initSessionsForUser = await _api.getCreatedSessionsForUser(authToken);
      _initSessionsForUser.sort((b, a) => a.end.compareTo(b.end));
      _sessionsForUser = _initSessionsForUser;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionsForUserFilter(String value) async {
    try {
      _sessionsForUser = _initSessionsForUser
          .where((item) =>
              // item.description.toLowerCase().contains(value.toLowerCase()) || auskommentiert aufgrund alter Daten
              item.id.toString().toLowerCase().contains(value.toLowerCase()) ||
              item.quiz.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionsForQuiz(Quiz quiz, Token authToken) async {
    try {
      _sessionsForQuiz = await _api.getSessionsForQuiz(quiz, authToken);
    } catch (e) {
      throw e;
    }
  }

  void deleteCourse(Course course, Token authToken) async {
    try {
      _api.deleteCourse(course, authToken);
    } catch (e) {
      throw e;
    }
  }

  void editCourse(Course course, Token authToken) async {
    try {
      _api.editCourse(course, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> checkIfOwner(int courseId, User user, Token authToken) async {
    try {
      Course courseInfo = await _api.getCourseInfo(courseId, authToken);
      isOwner = courseInfo.docent.id == user.id ? true : false;
    } catch (e) {
      throw e;
    }
  }

  Future<void> checkIfAdmin(Course course, User user, Token authToken) async {
    try {
      List<User> courseAdmins =
          await _api.getCourseAdmins(course.id, authToken);
      Course courseInfo = await _api.getCourseInfo(course.id, authToken);
      courseAdmins.add(courseInfo.docent);
      courseAdmins.forEach((item) {
        if (item.id == user.id) {
          isAdmin = true;
          return;
        }
      });
    } catch (e) {
      throw e;
    }
  }
}
