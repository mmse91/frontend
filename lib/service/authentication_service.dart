import 'package:flutter/foundation.dart';
import 'interfaces/i_api.dart';
import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/interfaces/i_api.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthenticationService {
  AuthenticationService({IApi api}) : _api = api;

  IApi _api;
  User _fetchedUser;
  final storage = new FlutterSecureStorage();
  bool _loggedIn = true;

  User get user => _fetchedUser;

  bool get loggedIn => _loggedIn;

  ///
  /// Tries to retrieve saved token from local storage. If successful, tries to get user information from it.
  Future<void> checkForToken() async {
    try {
      if (!kIsWeb) {
        _loggedIn = true;
        String token = await storage.read(key: "authToken");
        if (token != null) {
          _fetchedUser = await _api.getUserProfile(Token(token));
          _fetchedUser.authToken = Token(token);
        } else {
          _loggedIn = false;
          return;
        }
      }
    } catch (e) {
      _loggedIn = false;
      throw e;
    }
  }

  ///
  /// Login: get authentification token and save it in local storage.
  Future<void> login(String userIdText, String passwordText) async {
    try {
      _fetchedUser = await _api.getAuthToken(userIdText, passwordText);
      if (!kIsWeb) {
        await storage.write(
            key: "authToken", value: _fetchedUser.authToken.toString());
      }
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Logout: invalidate token and delete it from local storage.
  Future<void> logout() async {
    try {
      _loggedIn = false;
      await _api.invalidateAuthToken(_fetchedUser.authToken);
      _fetchedUser = null;
      if (!kIsWeb) {
        await storage.delete(key: "authToken");
      }
    } catch (e) {
      throw (e);
    }
  }
}
