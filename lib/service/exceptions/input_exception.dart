class InvalidCredentialsInputException extends Error {
  final message = 'Passwort besteht nur aus Zahlen';
}

class InvalidSessionIdInputException extends Error {
  final message = 'Eine Session ID besteht aus 8 Zahlen';
}
