import 'package:flutter/widgets.dart';
import 'package:fbs_app/domain/entities/index.dart';

abstract class IApi {
  /* ************* LOG-IN & -OUT ****************** */

  Future<User> getAuthToken(String username, String password);

  Future<void> invalidateAuthToken(Token authToken);

  /* ************* QUIZZE ***************** */

  Future<Session> getSessionToken(Session session, bool mode,
      [Token authToken]);

  Future<Session> getQuizInfo(int sessionId);

  Future<List<Question>> getSessionQuestions(Session session, Token sessToken);

  Future<Question> getSessionQuestion(
      Session session, int questionNumber, Token sessToken);

  Future<bool> submitAnswer(Session session, int questionNumber,
      Token sessToken, List<Answer> answers);

  /* ************* STATISTIK ***************** */

  Future<Statistics> getUserSessionStatistics(Session session, Token sessToken);

  Future<Statistics> getUserSessionStatisticsAuth(
      Session session, Token authToken);

  Future<List<QuestionStatistic>> getQuestionStatistics(
      Session session, Token authToken);

  Future<MemoryImage> getSessionStatisticsImage(
      Session session, Token authToken);

  Future<SessionStatisticsOverview> getSessionStatisticsOverview(
      Session session, Token authToken);

  Future<MemoryImage> getCourseStatisticsImage(Course course, Token authToken);

  Future<UserStatistics> getUserCourseStats(Token authToken, int courseId);
  Future<MemoryImage> getWordcloudImage(
      Session session, int questionNumber, Token authToken);

  /* ************* QUIZZE Verwalten ***************** */

  Future<Session> startSession(Quiz quiz, Session session, Token authToken);

  Future<Session> editSession(Session session, Token authToken);

  void deleteSession(Session session, Token authToken);

  Future<List<Session>> getSessionsForQuiz(Quiz quiz, Token authToken);

  Future<Quiz> submitQuiz(Quiz quiz, Token authToken);

  Future<Quiz> editQuiz(Quiz quiz, Token authToken);

  void deleteQuiz(Quiz quiz, Token authToken);

  Future<Question> submitQuestion(
      Quiz quiz, Question question, Token authToken);

  Future<Question> editQuestion(Quiz quiz, Question question, Token authToken);

  void deleteQuestion(Quiz quiz, Question question, Token authToken);

  Future<List<Question>> getQuestions(int quizId, Token authToken);

  /* ************* KURSE ****************** */

  Future<List<Course>> getAllCourses(Token authToken, bool hideJoined,
      [String search, int perPage, int page]);

  Future<Course> getCourseInfo(int courseId, Token authToken);

  Future<Course> joinCourse(int courseId, Token authToken, [int password]);

  void leaveCourse(int courseId, Token authToken);

  Future<Course> submitCourse(Course course, Token authToken);

  Future<Course> editCourse(Course course, Token authToken);

  void deleteCourse(Course course, Token authToken);

  Future<List<User>> getCourseAdmins(int courseId, Token authToken);

  Future<User> submitCourseAdmin(int courseId, User user, Token authToken);

  Future<User> deleteCourseAdmin(int courseId, User user, Token authToken);

  Future<Session> submitCourseSession(
      Course course, int sessionId, Token authToken);

  Future<List<Session>> getSessionsForCourse(int courseId, Token authToken);

  Future<List<User>> getCourseParticipants(int courseId, Token authToken,
      {bool hideAdmins});

  /* ************* BENUTZER ****************** */

  Future<List<Course>> getCoursesForUser(Token authToken, {String role});

  Future<List<Session>> getCreatedSessionsForUser(Token authToken,
      {bool hideEnded, bool hideNonStarted});

  Future<List<Session>> getDoneSessionsForUser(Token authToken);

  Future<List<Session>> getUndoneSessionsForUser(Token authToken);

  Future<List<Quiz>> getQuizzesForUser(Token authToken);

  Future<User> getUserProfile(Token authToken);

  Future<UserStatistics> getUserStats(Token authToken);
}
