import 'package:fbs_app/service/create_quiz_service.dart';
import 'package:fbs_app/service/quiz_service.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'data_source/api.dart';
import 'service/authentication_service.dart';
import 'service/session_service.dart';
import 'service/interfaces/i_api.dart';
import 'ui/router.dart';
import 'ui/common/app_theme.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Injector(
      inject: [
        //register services
        Inject<IApi>(() => Api()),
        Inject(() => AuthenticationService(api: Injector.get())),
        Inject(() => SessionService(api: Injector.get())),
        Inject(() => CreateQuizService(api: Injector.get())),
        Inject(() => QuizService(api: Injector.get())),
        Inject(() => UserSessionStatisticsService(api: Injector.get())),
        Inject(() => SessionStatisticsService(api: Injector.get()))
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Feedback App',
        theme: appTheme(),
        initialRoute: 'login',
        onGenerateRoute: Router.generateRoute,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const <Locale>[
          // all locales to be supported
          const Locale('de', 'DE'),
        ],
      ),
    );
  }
}
