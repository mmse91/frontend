import 'package:fbs_app/service/create_quiz_service.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/me/me_quizzes/quizzes_list_list.dart';
import 'package:fbs_app/ui/widgets/fbs_action_button.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/widgets/fbs_searchfield.dart';
import 'package:fbs_app/ui/widgets/webNavigationDrawer.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';

import 'package:fbs_app/ui/common/app_colors.dart';

class QuizzesListPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final TextEditingController searchController = new TextEditingController();

  Widget content(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: FBSAppBar(
            title: 'Meine Quizze', action: FbsActionButton(type: 'search')),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Injector.get<CreateQuizService>().comesFromHome = false;
            Navigator.pushNamed(context, 'quiz/newquiz');
          },
          child: Icon(
            Icons.add,
            color: white,
          ),
          backgroundColor: THMGrey,
        ),
        body: Center(
          child: Container(
            constraints:
                BoxConstraints(maxWidth: 850), // limit the width for Web
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
              child: Column(
                children: <Widget>[
                  WhenRebuilderOr<CoursesService>(
                    observe: () => RM.get<CoursesService>(),
                    initState: (_, coursesServiceRM) {
                      coursesServiceRM.setState(
                        (state) => state.search = false,
                      );
                    },
                    onWaiting: () {
                      return Container();
                    },
                    builder: (_, coursesService) {
                      return Visibility(
                        visible: coursesService.state.search,
                        child: Column(
                          children: <Widget>[
                            UIHelper.verticalSpaceSmall(),
                            FbsSearchField(
                              onChanged: (value) {
                                RM.get<CoursesService>().setState(
                                    (state) =>
                                        state.getQuizzesForUserFilter(value),
                                    onError: (context, e) {
                                  ErrorHandler.showErrorDialog(context, e);
                                });
                              },
                              textController: searchController,
                            )
                          ],
                        ),
                      );
                    },
                  ),
                  WhenRebuilderOr<CoursesService>(
                    observe: () => RM.get<CoursesService>(),
                    onError: (error) {
                      return Expanded(
                        child: Center(
                          child: Text(ErrorHandler.errorMessage(error),
                              textAlign: TextAlign.center),
                        ),
                      );
                    },
                    initState: (_, coursesServiceRM) {
                      coursesServiceRM.setState(
                          (state) => state.getQuizzesForUser(user.authToken),
                          onError: (context, e) {
                        ErrorHandler.showErrorDialog(context, e);
                      });
                    },
                    onWaiting: () {
                      return Expanded(
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(THMGreen),
                          ),
                        ),
                      );
                    },
                    builder: (_, coursesService) {
                      return QuizzesList(quizzes: coursesService.state.quizzes);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
            ? FbsBottomNavbar(index: 2)
            : null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => CoursesService(api: Injector.get()))],
        builder: (context) {
          return MediaQuery.of(context).size.width > 768
              ? WebNavigation(content(context), "me/quizzes")
              : //for web
              content(context); //for mobile
        });
  }
}
