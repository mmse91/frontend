import 'package:fbs_app/domain/entities/quiz.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/ui/common/app_theme.dart';

class QuizzesListItem extends StatelessWidget {
  final Quiz quiz;
  final Function onTap;

  const QuizzesListItem({this.quiz, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: quiz.name,
          subHeading: quiz.description,
          icon: quiz.survey == true
              ? Icon(
                  Icons.show_chart,
                  color: THMGrey,
                )
              : null,
        ));
  }
}
