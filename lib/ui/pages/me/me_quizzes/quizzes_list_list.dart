import 'package:fbs_app/domain/entities/quiz.dart';
import 'package:fbs_app/ui/pages/me/me_quizzes/quizzes_list_item.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';

class QuizzesList extends StatelessWidget {
  QuizzesList({this.quizzes});

  final List<Quiz> quizzes;

  @override
  Widget build(BuildContext context) {
    return quizzes.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: quizzes.length,
              itemBuilder: (context, index) => QuizzesListItem(
                quiz: quizzes[index],
                onTap: () {
                  Navigator.pushNamed(context, 'me/quizzes/detail',
                      arguments: quizzes[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Keine Quizze gefunden",
                style: headline3,
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpaceMedium(),
              FbsButton(
                  onPressed: () => Navigator.pushNamed(context, 'quiz/newquiz'),
                  text: "Quiz erstellen",
                  type: "primary",
                  size: "big")
            ],
          )));
  }
}
