import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/pages/me/me_sessions/me_sessions_detail/me_session_detail_actionrow.dart';
import 'package:fbs_app/ui/widgets/fbs_action_button.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:pie_chart/pie_chart.dart';

class SessionDetailPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final Session session;

  SessionDetailPage({this.session});

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [
          Inject(() => CoursesService(api: Injector.get())),
          Inject(() => SessionStatisticsService(api: Injector.get()))
        ],
        builder: (context) {
          return Scaffold(
            appBar: FBSAppBar(
              title: session.id.toString(),
              action: FbsActionButton(type: 'menu'),
            ),
            body: Align(
              alignment: Alignment.topCenter,
              child: Container(
                constraints:
                    BoxConstraints(maxWidth: 850), // limit the width for Web
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                  child: SingleChildScrollView(
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          WhenRebuilderOr<CoursesService>(
                            observe: () => RM.get<CoursesService>(),
                            initState: (_, coursesServiceRM) {
                              coursesServiceRM.setState(
                                (state) => state.options = true,
                              );
                            },
                            onWaiting: () {
                              return Container();
                            },
                            builder: (_, coursesService) {
                              return Visibility(
                                visible: coursesService.state.options,
                                child: Column(
                                  children: <Widget>[
                                    UIHelper.verticalSpaceMedium(),
                                    SessionDetailActionRow(session),
                                  ],
                                ),
                              );
                            },
                          ),
                          UIHelper.verticalSpaceMedium(),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Informationen',
                              style: headline4,
                            ),
                          ),
                          UIHelper.verticalSpaceSmall(),
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 3,
                                  blurRadius: 5,
                                  offset: Offset(
                                      0, 5), // changes position of shadow
                                ),
                              ],
                            ),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Beschreibung:",
                                  style: headline5,
                                ),
                                session.description != null
                                    ? Text(
                                        session.description != ""
                                            ? session.description
                                            : "Keine Beschreibung vorhanden",
                                        style: TextStyle(color: font_dark))
                                    : Text("Keine Beschreibung (null)",
                                        style: TextStyle(color: font_dark)),
                                SizedBox(height: 10),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Start:", style: headline5),
                                    Text(
                                        session
                                            .getLocaleDatetime(session.start),
                                        style: TextStyle(color: font_dark)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Ende:", style: headline5),
                                    Text(session.getLocaleDatetime(session.end),
                                        style: TextStyle(color: font_dark)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Quiz:", style: headline5),
                                    Text(session.quiz.name.trim(),
                                        style: TextStyle(color: font_dark)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Text(
                                        "Kurs:",
                                        style: headline5,
                                      ),
                                    ),
                                    Expanded(
                                      //width:
                                      //  MediaQuery.of(context).size.width * 0.5,
                                      child: Text(
                                        session.course != null
                                            ? session.course.name
                                            : 'Keinem Kurs zugeordnet',
                                        style: TextStyle(color: font_dark),
                                        textAlign: TextAlign.right,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Typ:", style: headline5),
                                    Text(
                                        session.quiz.survey == true
                                            ? 'Umfrage'
                                            : 'Quiz',
                                        style: TextStyle(color: font_dark)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Modus:", style: headline5),
                                    Text(
                                        session.getMode() == true
                                            ? 'Anonym'
                                            : 'Angemeldet',
                                        style: TextStyle(color: font_dark)),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          UIHelper.verticalSpaceMedium(),
                          session.quiz.survey
                              ? Container()
                              : Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Statistiken',
                                        textAlign: TextAlign.start,
                                        style: headline4,
                                      ),
                                    ),
                                    UIHelper.verticalSpaceSmall(),
                                    WhenRebuilderOr<SessionStatisticsService>(
                                      observe: () =>
                                          RM.get<SessionStatisticsService>(),
                                      initState:
                                          (_, sessionStatisticsServiceRM) {
                                        sessionStatisticsServiceRM.setState(
                                            (state) =>
                                                state.getSessionStatsOverview(
                                                    session, user.authToken),
                                            onError: (context, e) {
                                          ErrorHandler.showErrorDialog(
                                              context, e);
                                        });
                                      },
                                      onWaiting: () {
                                        return Container();
                                      },
                                      onError: (error) => Center(
                                          child: Text(
                                              'Leider ist hier ein Fehler aufgetreten',
                                              style: headline5)),
                                      builder: (_, sessionStatisticsService) {
                                        return Container(
                                          padding: EdgeInsets.all(20),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(12),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.1),
                                                spreadRadius: 3,
                                                blurRadius: 5,
                                                offset: Offset(0,
                                                    5), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  'Richtig beantwortete Fragen',
                                                  style: headline5,
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                              UIHelper.verticalSpaceSmall(),
                                              sessionStatisticsService.state
                                                          .stats.completed >
                                                      0
                                                  ? PieChart(
                                                      dataMap:
                                                          sessionStatisticsService
                                                              .state
                                                              .stats
                                                              .chartData,
                                                      colorList: [
                                                        THMGreen,
                                                        bg_green,
                                                        yellow,
                                                        Colors.red[300]
                                                      ],
                                                      legendStyle: TextStyle(
                                                          color: font_dark,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 12),
                                                      showChartValuesInPercentage:
                                                          false,
                                                    )
                                                  : Text(
                                                      "Es wurde noch keine Session abgeschlossen.",
                                                      style: TextStyle(
                                                          color: font_dark)),
                                              UIHelper.verticalSpaceSmall(),
                                              Flex(
                                                direction:
                                                    MediaQuery.of(context)
                                                                .size
                                                                .width >
                                                            300
                                                        ? Axis.horizontal
                                                        : Axis.vertical,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  RichText(
                                                    text: TextSpan(
                                                      text: 'Teilnehmer: ',
                                                      style: headline5,
                                                      children: <TextSpan>[
                                                        TextSpan(
                                                            text:
                                                                sessionStatisticsService
                                                                    .state
                                                                    .stats
                                                                    .participants
                                                                    .toString(),
                                                            style: null),
                                                      ],
                                                    ),
                                                  ),
                                                  RichText(
                                                    text: TextSpan(
                                                      text: 'Abgeschlossen: ',
                                                      style: headline5,
                                                      children: <TextSpan>[
                                                        TextSpan(
                                                            text:
                                                                sessionStatisticsService
                                                                    .state
                                                                    .stats
                                                                    .completed
                                                                    .toString(),
                                                            style: null),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                          UIHelper.verticalSpaceMedium(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 2)
                : null,
          );
        });
  }
}
