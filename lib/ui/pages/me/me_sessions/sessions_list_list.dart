import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/ui/pages/me/me_sessions/sessions_list_item.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/ui/common/text_styles.dart';

class SessionsList extends StatelessWidget {
  SessionsList({this.sessions});

  final List<Session> sessions;

  @override
  Widget build(BuildContext context) {
    return sessions.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: sessions.length,
              itemBuilder: (context, index) => SessionsListItem(
                session: sessions[index],
                onTap: () {
                  Navigator.pushNamed(context, 'me/sessions/detail',
                      arguments: sessions[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Keine Sessions gefunden",
                style: headline3,
                textAlign: TextAlign.center,
              ),
            ],
          )));
  }
}
