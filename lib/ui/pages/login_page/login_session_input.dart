import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';

class LoginSessionInput extends StatelessWidget {
  final TextEditingController sessionController;

  LoginSessionInput({@required this.sessionController});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Container(
        constraints: BoxConstraints(maxHeight: 245),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Padding(
            padding: EdgeInsets.all(25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    'Lege direkt los:',
                    textAlign: TextAlign.start,
                    style: headline4,
                  ),
                ),
                UIHelper.verticalSpaceSmall(),
                Container(
                  child: TextFormField(
                    controller: sessionController,
                    cursorColor: THMGreen,
                    onFieldSubmitted: (term) {
                      if (sessionController.text.isNotEmpty) {
                        RM.get<SessionService>().setState(
                            (state) => state.getSession(sessionController.text),
                            onData: (context, sessServiceRM) {
                          if (sessServiceRM.session != null) {
                            Navigator.pushNamed(context, 'quiz/begin',
                                arguments: sessServiceRM.session);
                          }
                        }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                      }
                    },
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      hintText: 'Session-ID',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide()),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: THMGreen, width: 2.0),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                ),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<SessionService>(),
                  builder: (_, __) => Container(),
                ),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr<SessionService>(
                  observe: () => RM.get<SessionService>(),
                  onWaiting: () => Center(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(green),
                    ),
                  ),
                  dispose: (_, __) {
                    sessionController.dispose();
                  },
                  builder: (_, sessServiceRM) {
                    return Center(
                      child: FbsButton(
                        text: "OK",
                        type: 'primary',
                        size: "big",
                        onPressed: () {
                          if (sessionController.text.isNotEmpty) {
                            sessServiceRM.setState(
                                (state) =>
                                    state.getSession(sessionController.text),
                                onData: (context, sessServiceRM) {
                              if (sessServiceRM.session != null) {
                                Navigator.pushNamed(context, 'quiz/begin',
                                    arguments: sessServiceRM.session);
                              }
                            }, onError: (context, e) {
                              ErrorHandler.showErrorDialog(context, e);
                            });
                          }
                        },
                      ),
                    );
                  },
                )
              ],
            )),
      ),
    );
  }
}
