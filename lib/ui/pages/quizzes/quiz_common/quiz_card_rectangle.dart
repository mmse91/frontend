import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:flutter/material.dart';

class QuizCardRectangleState extends StatefulWidget {
  QuizCardRectangleState({this.text, this.tooltip, this.chosen});

  final String text;
  final String tooltip;
  bool chosen;

  _QuizCardRectangle createState() => _QuizCardRectangle();
}

class _QuizCardRectangle extends State<QuizCardRectangleState> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          widget.chosen = !widget.chosen;
        });
      },
      child: Container(
          height: 90,
          margin: EdgeInsets.symmetric(vertical: 6),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: (widget.chosen) ? green : Colors.white,
            ),
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: Text(widget.text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                    )),
              ))),
    );
  }
}
