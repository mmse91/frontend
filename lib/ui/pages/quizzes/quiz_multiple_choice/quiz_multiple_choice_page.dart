import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/domain/entities/question/question.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class QuizMultipleChoicePage extends StatefulWidget {
  final MultipleChoiceQuestion question;
  final void Function(Question q) onAnswerSelect;
  final Key key;

  QuizMultipleChoicePage(this.question, this.onAnswerSelect, this.key);

  _QuizMultipleChoicePageState createState() =>
      _QuizMultipleChoicePageState(question, onAnswerSelect);
}

class _QuizMultipleChoicePageState extends State<QuizMultipleChoicePage> {
  final void Function(Question q)
      onQuestionChanged; //callback to parent: notify that answer has changed
  final MultipleChoiceQuestion mcQuestion;

  _QuizMultipleChoicePageState(this.mcQuestion, this.onQuestionChanged);

  void onAnswerChanged(MultipleChoiceAnswer a, int index) {
    mcQuestion.answers[index] = a; //changed chosen
    onQuestionChanged(mcQuestion);
  }

  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: Text("Wähle die richtigen Antworten."),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: Text(mcQuestion.question,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: font_dark,
                    )),
              )
            ],
          ),
        ),
        (mcQuestion.answers != null)
            ? Column(
                children: <Widget>[
                  QuizCardMCState(mcQuestion.answers[0], onAnswerChanged, 0),
                  QuizCardMCState(mcQuestion.answers[1], onAnswerChanged, 1),
                  QuizCardMCState(mcQuestion.answers[2], onAnswerChanged, 2),
                  QuizCardMCState(mcQuestion.answers[3], onAnswerChanged, 3),
                ],
              )
            : Container(),
      ],
    );
  }
}

class QuizCardMCState extends StatefulWidget {
  QuizCardMCState(this.answer, this.onAnswerSelect, this.index);

  final void Function(MultipleChoiceAnswer a, int index) onAnswerSelect;
  final MultipleChoiceAnswer answer;
  final int index;

  _QuizCardMC createState() => _QuizCardMC(answer, onAnswerSelect, index);
}

class _QuizCardMC extends State<QuizCardMCState> {
  final MultipleChoiceAnswer mcanswer;

  _QuizCardMC(this.mcanswer, this.onAnswerSelect, this.index);

  final int index;
  final void Function(MultipleChoiceAnswer a, int index) onAnswerSelect;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      onTap: () {
        setState(() {
          mcanswer.chosen = !mcanswer.chosen;
          onAnswerSelect(mcanswer, this.index);
        });
      },
      child: Container(
          height: 90,
          margin: EdgeInsets.symmetric(vertical: 6),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: (mcanswer.chosen) ? green : Colors.white,
            ),
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 5),
              ),
            ],
          ),
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: Text(mcanswer.answer,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                    )),
              ))),
    );
  }
}
