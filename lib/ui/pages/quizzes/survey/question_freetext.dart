import 'package:fbs_app/domain/entities/question/openQuestion.dart';
import 'package:fbs_app/domain/entities/question/question.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/common/app_colors.dart';

class OpenQuestionPage extends StatefulWidget {
  final OpenQuestion question;
  final void Function(Question q) onAnswerChanged;
  final Key key;

  OpenQuestionPage(this.question, this.onAnswerChanged, this.key);

  _OpenQuestionState createState() =>
      _OpenQuestionState(this.question, this.onAnswerChanged, this.key);
}

class _OpenQuestionState extends State<OpenQuestionPage> {
  _OpenQuestionState(this.question, this.onQuestionChanged, this.key);

  final void Function(Question q)
      onQuestionChanged; //callback to parent: notify that answer has changed
  final OpenQuestion question;
  final Key key;

  void answerChanged(String text) {
    OpenAnswer ans = new OpenAnswer(text);
    this.question.answers = [ans];
    onQuestionChanged(question);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      key: key,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: Text("Beantworte die Frage mit einem Text."),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: Text(question.question,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: font_dark,
                    )),
              )
            ],
          ),
        ),
        UIHelper.verticalSpaceLarge(),
        Container(
            padding: EdgeInsets.fromLTRB(0, 25, 0, 25),
            constraints: BoxConstraints(
                minHeight: 100,
                maxHeight: MediaQuery.of(context).size.height / 3),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 3,
                        blurRadius: 5,
                        offset: Offset(0, 5),
                      ),
                    ]),
                child: TextField(
                  textAlignVertical: TextAlignVertical.top,
                 // expands: true,
                  //keyboardType: TextInputType.multiline,
                  maxLines: 2,
                  maxLength: 64,
                  decoration: InputDecoration(
                    counterText: "",
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: green,
                          style: BorderStyle.solid,
                        )),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: green,
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                  onChanged: (text) {
                    answerChanged(text);
                  },
                ))),
      ],
    );
  }
}
