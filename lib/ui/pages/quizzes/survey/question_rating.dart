import 'package:fbs_app/domain/entities/question/question.dart';
import 'package:fbs_app/domain/entities/question/ratingQuestion.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/common/app_colors.dart';

class RatingQuestionPage extends StatefulWidget {
  final RatingQuestion question;
  final void Function(Question q) onAnswerChanged;
  final Key key;

  RatingQuestionPage(this.question, this.onAnswerChanged, this.key);

  _RatingQuestionState createState() =>
      _RatingQuestionState(this.question, this.onAnswerChanged, this.key);
}

class _RatingQuestionState extends State<RatingQuestionPage> {
  _RatingQuestionState(this.question, this.onQuestionChanged, this.key);

  final void Function(Question q)
      onQuestionChanged; //callback to parent: notify that answer has changed
  final RatingQuestion question;
  final Key key;
  int choice = 3;

  void answerChanged(String desc) {
    RatingAnswer ans = new RatingAnswer(choice, desc);
    List<RatingAnswer> answers = [ans];
    question.answers = answers;
    onQuestionChanged(question);
  }

  @override
  Widget build(BuildContext context) {
    answerChanged('Neutral');
    return Column(
      key: key,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: Text("Wähle die zutreffende Bewertung aus."),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: Text(question.question,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: font_dark,
                    )),
              )
            ],
          ),
        ),
        Column(
          children: [
            RadioListTile<int>(
                title: const Text('Trifft voll zu'),
                activeColor: green,
                value: 1,
                groupValue: choice,
                onChanged: (int value) {
                  setState(() {
                    choice = value;
                    answerChanged('Trifft voll zu');
                  });
                }),
            RadioListTile<int>(
                title: const Text('Trifft zu'),
                value: 2,
                activeColor: green,
                groupValue: choice,
                onChanged: (int value) {
                  setState(() {
                    choice = value;
                    answerChanged('Trifft zu');
                  });
                }),
            RadioListTile<int>(
                title: const Text('Neutral'),
                value: 3,
                activeColor: green,
                groupValue: choice,
                onChanged: (int value) {
                  setState(() {
                    choice = value;
                    answerChanged('Neutral');
                  });
                }),
            RadioListTile<int>(
                title: const Text('Trifft nicht zu'),
                value: 4,
                activeColor: green,
                groupValue: choice,
                onChanged: (int value) {
                  setState(() {
                    choice = value;
                    answerChanged('Trifft nicht zu');
                  });
                }),
            RadioListTile<int>(
                title: const Text('Trifft überhaupt nicht zu'),
                value: 5,
                activeColor: green,
                groupValue: choice,
                onChanged: (int value) {
                  setState(() {
                    choice = value;
                    answerChanged('Trifft überhaupt nicht zu');
                  });
                }),
          ],
        )
      ],
    );
  }
}
