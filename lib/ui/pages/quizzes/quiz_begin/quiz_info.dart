import 'package:fbs_app/domain/entities/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/domain/entities/session.dart';

class QuizInfo extends StatelessWidget {
  final Session session;

  QuizInfo(this.session);

  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: (session.quiz.name != null)
            ? Text(
                '${session.quiz.description}\n\nErsteller: ${session.creator}\n\nTeilnahme vom ${session.startDate} bis ${session.endDate} möglich\n\nFragenanzahl: ${session.quiz.questionCount}\n\nSession-Id: ${session.id}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  color: greyLetters,
                ),
              )
            : Container(),
      ),
    );
  }
}
