import 'package:fbs_app/domain/exceptions/validation_exception.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_begin/quiz_info.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_common/quiz_header.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class QuizStartPage extends StatefulWidget {
  QuizStartPage(this.currentSession);

  final Session currentSession;

  _QuizStart createState() => _QuizStart(currentSession);
}

class _QuizStart extends State<QuizStartPage> {
  void initState() {
    super.initState();
  }

  final user = Injector.get<AuthenticationService>().user;
  final session;

  _QuizStart(this.session);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final _scrollController = ScrollController();
    return Scaffold(
      backgroundColor: theme.backgroundColor,
      body: SafeArea(
        child: Center(
            child: Container(
          padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
          constraints: ((screenSize.width) > 768)
              ? BoxConstraints(minWidth: 100, maxWidth: 500)
              : null,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              QuizHeader(
                currentQuestion: 0,
                countQuestion: session.quiz.questionCount,
                quizName: session.quiz.name,
              ),
              Expanded(
                  child: Scrollbar(
                      isAlwaysShown: screenSize.aspectRatio >= 0.6,
                      controller: _scrollController,
                      child: SingleChildScrollView(
                          controller: _scrollController,
                          child: Column(children: [
                            Container(
                              padding: const EdgeInsets.only(top: 30),
                              child: Center(
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: white,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: theme.highlightColor,
                                          style: BorderStyle.solid,
                                          width: 1.5),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 5,
                                          offset: Offset(0, 5),
                                        ),
                                      ]),
                                  child: Padding(
                                    padding: const EdgeInsets.all(30.0),
                                    child: Icon(
                                      Icons.lightbulb_outline,
                                      color: theme.highlightColor,
                                      size: screenSize.aspectRatio * 150,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            QuizInfo(session)
                          ])))),
              UIHelper.verticalSpaceMedium(),
              WhenRebuilderOr<SessionService>(
                  observe: () => RM.get<SessionService>(),
                  onWaiting: () => Center(
                        child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(green),
                        ),
                      ),
                      builder: (_, sessServiceRM) {
                        return Container(
                            child: FbsButton(
                              text: 'Starten',
                              type: "secondary",
                              size: "big",
                              onPressed: () {
                        sessServiceRM.setState(
                            (state) => state.getSessionToken(
                                session, user == null ? null : user.authToken),
                            onData: (context, sessServiceRM) {
                              if (sessServiceRM.session != null &&
                                  session.sessToken != null &&
                                  session.quiz.questionCount > 0) {
                                Navigator.pushReplacementNamed(
                                    context, 'quiz/active',
                                    arguments: sessServiceRM.session);
                              } else if (session.quiz.questionCount <= 0) {
                                ErrorHandler.showErrorDialog(
                                    context,
                                    ValidationException(
                                        'Das Quiz zu dieser Session hat keine Fragen.'));
                              }
                            }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                              },
                            ));
                      }),
                  UIHelper.verticalSpaceMedium()
                ],
              ),
            )),
      ),
    );
  }
}
