import 'package:fbs_app/domain/entities/index.dart';
import 'package:flutter/material.dart';

class QuizOrderPage extends StatefulWidget {
  QuizOrderPage(this.question, this.onQuestionChanged, this.key);

  final SortQuestion question;
  final Key key;
  final void Function(Question q) onQuestionChanged;

  _QuizOrderPageScreen createState() =>
      _QuizOrderPageScreen(question, onQuestionChanged);
}

//Draggable Antworten - Anwendung: Reihenfolge
class DragAnswer extends StatelessWidget {
  final String answer;

  DragAnswer(this.answer);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          margin: EdgeInsets.fromLTRB(25, 10, 25, 10),
          padding: EdgeInsets.all(25),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          child: Center(
            child: Text(
              answer,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16),
            ),
          )),
    );
  }
}

class _QuizOrderPageScreen extends State<QuizOrderPage> {
// a is target
  SortQuestion question;
  final void Function(Question q) onQuestionChanged;

  _QuizOrderPageScreen(this.question, this.onQuestionChanged);

  List<DragAnswer> answers;

  // a is target
  Widget _buildDragTarget(BuildContext context, DragAnswer a, int no) {
    return DragTarget(
        builder: (context, List<DragAnswer> candidateData, rejectedData) {
      return Draggable(
        child: a,
        data: a,
        feedback: a,
      );
    }, onWillAccept: (data) {
      return true;
    }, onAccept: (data) {
      if (a != data) {
        //swap Answers
        int tempIndexA = answers.indexOf(a);
        answers[answers.indexOf(data)] = a;
        answers[tempIndexA] = data;
        setState(() {
          question.answers.clear();
          question.answers.add(new SortAnswer(answers[0].answer));
          question.answers.add(new SortAnswer(answers[1].answer));
          question.answers.add(new SortAnswer(answers[2].answer));
          question.answers.add(new SortAnswer(answers[3].answer));
          onQuestionChanged(question);
        });
      }
    });
  }

  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey();

  Widget build(BuildContext context) {
    answers = [
      new DragAnswer(question.answers[0].answer),
      new DragAnswer(question.answers[1].answer),
      new DragAnswer(question.answers[2].answer),
      new DragAnswer(question.answers[3].answer)
    ];
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child:
                    Text("Ordne die Begriffe in der richtigen Reihenfolge an."),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                child: question != null
                    ? Text(question.question,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF394A59),
                        ))
                    : Container(),
              )
            ],
          ),
        ),
        (answers != null)
            ? Column(
                children: <Widget>[
                  _buildDragTarget(context, answers[0], 0),
                  _buildDragTarget(context, answers[1], 1),
                  _buildDragTarget(context, answers[2], 2),
                  _buildDragTarget(context, answers[3], 3),
                ],
              )
            : Container(child: Text("Frage wird geladen...")),
      ],
    );
  }
}
