import 'package:fbs_app/domain/entities/quiz.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/create_quiz_service.dart';
import 'package:fbs_app/service/quiz_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_options_card.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class QuizDetailActionRow extends StatelessWidget {
  QuizDetailActionRow(this.quiz);

  final Quiz quiz;
  final user = Injector.get<AuthenticationService>().user;

  createDeleteQuizDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Quiz löschen'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Willst du dieses Quiz wirklich löschen?"),
                UIHelper.verticalSpaceSmall(),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Nein', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              WhenRebuilderOr<QuizService>(
                observe: () => RM.get<QuizService>(),
                onWaiting: () => Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                  ),
                ),
                builder: (_, quizServiceRM) {
                  return Center(
                    child: FlatButton(
                      child: Text('Ja, löschen',
                          style: TextStyle(color: font_dark)),
                      onPressed: () {
                        quizServiceRM.setState(
                          (state) => state.deleteQuiz(quiz, user.authToken),
                          onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          },
                          onData: (context, quizServiceRM) {
                            Navigator.pop(context); //close dialog
                            Navigator.pop(context);
                            Navigator.pushReplacementNamed(
                                context, 'me/quizzes');
                          },
                        );
                      },
                    ),
                  );
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(context, 'quiz/newsession',
                arguments: this.quiz),
            iconData: Icons.add,
            title: 'Session erstellen'),
        SizedBox(width: 20),
        WhenRebuilderOr<QuizService>(
          observe: () => RM.get<QuizService>(),
          builder: (_, quizServiceRM) {
            return FbsOptionsCard(
                onTap: () {
                  try {
                    quizServiceRM.setState(
                        (state) =>
                            state.getQuestions(this.quiz, user.authToken),
                        onData: (context, quizServiceRM) {
                      Injector.get<CreateQuizService>().comesFromHome = false;
                      Navigator.pushReplacementNamed(context, 'quiz/edit',
                          arguments: quizServiceRM.quiz);
                    });
                  } catch (error) {
                    ErrorHandler.showErrorDialog(context, error);
                  }
                },
                iconData: Icons.edit,
                title: 'Quiz bearbeiten');
          },
        ),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => createDeleteQuizDialog(context),
            iconData: Icons.delete,
            title: 'Quiz \nlöschen'),
      ],
    );
  }
}
