import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_common/quiz_header.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class QuizEndPage extends StatelessWidget {
  final session = Injector.get<SessionService>().session;
  final course = Injector.get<SessionService>().course;
  final user = Injector.get<AuthenticationService>().user;
  final bool survey;

  QuizEndPage(this.survey);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final _scrollController = ScrollController();
    return Scaffold(
      backgroundColor: theme.backgroundColor,
      body: SafeArea(
          child: Center(
        child: Container(
          padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
          constraints: ((MediaQuery.of(context).size.width) > 768)
              ? BoxConstraints(minWidth: 100, maxWidth: 500)
              : null,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              QuizHeader(
                  currentQuestion: session.quiz.questionCount,
                  countQuestion: session.quiz.questionCount,
                  quizName: session.quiz.name),
              Expanded(
                child: Scrollbar(
                  isAlwaysShown: screenSize.aspectRatio >= 0.6,
                  controller: _scrollController,
                  child: SingleChildScrollView(
                    controller: _scrollController,
                    child: Container(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 30),
                              child: Center(
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: white,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: theme.highlightColor,
                                          style: BorderStyle.solid,
                                          width: 1.0),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 5,
                                          offset: Offset(0, 5),
                                        ),
                                      ]),
                                  child: Padding(
                                    padding: const EdgeInsets.all(30.0),
                                    child: Icon(
                                      Icons.done_all,
                                      color: theme.highlightColor,
                                      size: screenSize.aspectRatio * 150,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            UIHelper.verticalSpaceMedium(),
                            Text(
                              survey
                                  ? 'Die Umfrage wurde erfolgreich beendet.'
                                  : 'Das Quiz wurde erfolgreich beendet.',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 30,
                                color: greyLetters,
                              ),
                            ),
                          ]),
                    ),
                  ),
                ),
              ),
              UIHelper.verticalSpaceSmall(),
              FbsButton(
                onPressed: () {
                  if (!survey)
                    Navigator.pushReplacementNamed(context, 'statistics/user');
                  else {
                    Navigator.pop(context);
                    RM.get<SessionService>().refresh();
                    if (user == null) {
                      Navigator.pushReplacementNamed(context, 'login');
                    } else {
                      course != null
                          ? Navigator.pushReplacementNamed(
                              context, 'courses/detail',
                              arguments: course)
                          : Navigator.pushReplacementNamed(context, 'home');
                    }
                  }
                },
                text: survey ? 'Beenden' : 'Zum Ergebnis',
                size: 'big',
                type: 'primary',
              ),
              UIHelper.verticalSpaceMedium()
            ],
          ),
        ),
      )),
    );
  }
}
