import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail_admin/course_detail_admin_sessions_list_item.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';

class CoursesDetailAdminSessionsList extends StatelessWidget {
  CoursesDetailAdminSessionsList({this.sessions});

  final List<Session> sessions;

  @override
  Widget build(BuildContext context) {
    return sessions.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: sessions.length,
              itemBuilder: (context, index) =>
                  CoursesDetailAdminSessionsListItem(
                session: sessions[index],
                onTap: () {
                  Navigator.pushNamed(context, 'me/sessions/detail',
                      arguments: sessions[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Der Kurs hat noch keine Session",
                style: headline3,
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpaceMedium(),
              FbsButton(
                  onPressed: () => Navigator.pushNamed(context, 'me/quizzes'),
                  text: "Session erstellen",
                  type: "primary",
                  size: "big")
            ],
          )));
  }
}
