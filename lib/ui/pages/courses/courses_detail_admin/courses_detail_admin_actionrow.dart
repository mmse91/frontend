import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/widgets/fbs_options_card.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class CoursesDetailAdminActionRow extends StatelessWidget {
  CoursesDetailAdminActionRow(this.course);

  final Course course;
  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FbsOptionsCard(
          title: 'Statistiken',
          iconData: Icons.poll,
          onTap: () => Navigator.pushNamed(context, 'statistics/course',
              arguments: course),
        ),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(context, 'courses/detail/students',
                arguments: course),
            iconData: Icons.people,
            title: 'Teilnehmer'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () =>
                Navigator.pushNamed(context, 'quiz/newquiz', arguments: course),
            iconData: Icons.add,
            title: 'Neues Quiz')
      ],
    );
  }
}
