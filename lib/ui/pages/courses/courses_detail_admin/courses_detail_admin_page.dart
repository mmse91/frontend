import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail_admin/course_detail_admin_list.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail_admin/courses_detail_admin_actionrow.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail_admin/courses_detail_admin_owner_actionrow.dart';
import 'package:fbs_app/ui/widgets/fbs_action_button.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';

import '../../../common/ui_helpers.dart';

class CoursesDetailAdminPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;

  final Course course;

  CoursesDetailAdminPage({this.course});

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => CoursesService(api: Injector.get()))],
        builder: (context) {
          return Scaffold(
            appBar: FBSAppBar(
                title: course.name,
                action: course.docent.id == user.id
                    ? FbsActionButton(type: 'settings')
                    : null),
            body: Center(
              child: Container(
                constraints:
                    BoxConstraints(maxWidth: 850), // limit the width for Web
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                  child: Column(
                    children: <Widget>[
                      UIHelper.verticalSpaceMedium(),
                      CoursesDetailAdminActionRow(course),
                      WhenRebuilderOr<CoursesService>(
                        observe: () => RM.get<CoursesService>(),
                        initState: (_, coursesServiceRM) {
                          coursesServiceRM.state
                              .checkIfOwner(course.id, user, user.authToken);
                          coursesServiceRM
                              .setState((state) => state.options = false,
                                  onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          });
                        },
                        onWaiting: () {
                          return Container();
                        },
                        builder: (_, coursesService) {
                          return Visibility(
                            visible: coursesService.state.options,
                            child: Column(
                              children: <Widget>[
                                UIHelper.verticalSpaceMedium(),
                                CoursesDetailAdminOwnerActionRow(course),
                              ],
                            ),
                          );
                        },
                      ),
                      UIHelper.verticalSpaceMedium(),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Sessions',
                          textAlign: TextAlign.start,
                          style: headline4,
                        ),
                      ),
                      WhenRebuilderOr<CoursesService>(
                        observe: () => RM.get<CoursesService>(),
                        initState: (_, coursesServiceRM) {
                          coursesServiceRM.setState(
                              (state) => state.getSessionsForCourse(
                                  course.id, user.authToken),
                              onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          });
                        },
                        onError: (error) {
                          return Expanded(
                            child: Center(
                              child: Text(ErrorHandler.errorMessage(error),
                                  textAlign: TextAlign.center),
                            ),
                          );
                        },
                        onWaiting: () {
                          return Expanded(
                            child: Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation<Color>(THMGreen),
                              ),
                            ),
                          );
                        },
                        builder: (_, coursesService) {
                          return CoursesDetailAdminSessionsList(
                              sessions: coursesService.state.sessions);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 2)
                : null,
          );
        });
  }
}
