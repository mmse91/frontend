import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class ListHeader extends StatelessWidget {
  ListHeader({this.pageTitle});

  final String pageTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      child: Container(
        transform: Matrix4.translationValues(0.0, 8.0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              onTap: () => Navigator.pushNamed(context, 'courses/findCourse'),
              child: CircleAvatar(
                backgroundColor: bg_dark,
                radius: 20,
                child: Icon(Icons.add, color: Colors.white),
              ),
            ),
            Text(pageTitle, style: headline3),
            InkWell(
              onTap: () {
                RM.get<CoursesService>().setState(
                      (state) => state.search = !state.search,
                    );
              },
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 20,
                child: Icon(
                  Icons.search,
                  color: bg_dark,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
