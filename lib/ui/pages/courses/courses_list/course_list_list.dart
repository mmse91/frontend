import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/pages/courses/courses_list/courses_list_item.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import '../../../common/text_styles.dart';
import '../../../common/ui_helpers.dart';
import '../../../widgets/fbs_button.dart';

class CourseList extends StatelessWidget {
  CourseList({this.courses});

  final List<Course> courses;

  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    return courses.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: courses.length,
              itemBuilder: (context, index) => CourseListItem(
                course: courses[index],
                onTap: () {
                  Navigator.pushNamed(context, 'courses/detail',
                      arguments: courses[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Zu deiner Suche wurden keine Kurse gefunden",
                style: headline3,
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpaceMedium(),
              FbsButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, 'courses/findCourse'),
                  text: "Kurs hinzufügen",
                  type: "primary",
                  size: "big"),
              UIHelper.verticalSpaceMedium(),
              user.docent
                  ? FbsButton(
                      onPressed: () =>
                          Navigator.pushNamed(context, 'me/courses'),
                      text: "Zu meinen Kursen wechseln",
                      type: "secondary",
                      size: "big")
                  : Container(),
            ],
          )));
  }
}
