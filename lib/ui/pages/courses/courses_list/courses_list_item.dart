import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/domain/entities/course.dart';

import '../../../common/app_theme.dart';

class CourseListItem extends StatelessWidget {
  final Course course;
  final Function onTap;

  const CourseListItem({this.course, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: course.name,
          subHeading: course.semester,
          icon: course.type == 'admin' || course.type == 'owner'
              ? Icon(
                  Icons.school,
                  color: THMGrey,
                )
              : null,
        ));
  }
}
