import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail/course_detail_sessions_list_item.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class CourseSessionsList extends StatelessWidget {
  CourseSessionsList({this.sessions, this.course});

  final List<Session> sessions;
  final Course course;

  final user = Injector.get<AuthenticationService>().user;
  final sessService = Injector.get<SessionService>();

  @override
  Widget build(BuildContext context) {
    return sessions.isNotEmpty
        ? WhenRebuilderOr<UserSessionStatisticsService>(
            observe: () => RM.get<UserSessionStatisticsService>(),
            builder: (_, usServiceRM) {
              return Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.only(top: 8),
                  shrinkWrap: true,
                  itemCount: sessions.length,
                  itemBuilder: (context, index) => CourseSessionsListItem(
                    session: sessions[index],
                    onTap: () {
                      if (sessions[index].complete) {
                        if (!sessions[index].quiz.survey) {
                          sessService.setSession(sessions[index]);
                          sessService.course = course;
                          usServiceRM.setState(
                              (state) => state.getStats(sessions[index],
                                  authToken: user.authToken),
                              onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          }, onData: (context, usServiceRM) {
                            Navigator.pushNamed(context, 'statistics/user');
                          });
                        }
                      } else if (sessions[index].end * 1000 >=
                          new DateTime.now().millisecondsSinceEpoch) {
                        sessService.course = course;
                        Navigator.pushNamed(context, 'quiz/begin',
                            arguments: sessions[index]);
                      }
                    },
                  ),
                ),
              );
            },
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Dieser Kurs hat noch keine Session",
                style: headline3,
                textAlign: TextAlign.center,
              ),
            ],
          )));
  }
}
