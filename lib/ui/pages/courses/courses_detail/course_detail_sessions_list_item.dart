import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

class CourseSessionsListItem extends StatelessWidget {
  final Session session;
  final Function onTap;

  CourseSessionsListItem({this.session, this.onTap});

  @override
  Widget build(BuildContext context) {
    var icon;
    if (session.complete) {
      icon =
          session.quiz.survey ? null : Icon(Icons.insert_chart, color: THMGrey);
    } else if (session.end * 1000 < new DateTime.now().millisecondsSinceEpoch) {
      icon = Icon(Icons.clear, color: THMGrey);
    } else {
      icon = Icon(Icons.fast_forward, color: THMGrey);
    }
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: session.quiz.name,
          subHeading: session.end * 1000 <
                  new DateTime.now().millisecondsSinceEpoch
              ? session.complete
                  ? 'Bereits abgeschlossen'
                  : 'Geschlossen seit: ${session.getLocaleDatetime(session.end)}'
              : session.complete
                  ? 'Bereits abgeschlossen'
                  : 'Geöffnet bis: ${session.getLocaleDatetime(session.end)}',
          icon: icon,
        ));
  }
}
