import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:flutter/material.dart';

import '../../../common/app_colors.dart';

class FindCoursesListHeader extends StatelessWidget {
  FindCoursesListHeader({this.pageTitle});

  final String pageTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      child: Container(
        transform: Matrix4.translationValues(0.0, 8.0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 40,
            ),
            Text(pageTitle, style: headline3),
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: CircleAvatar(
                backgroundColor: bg_dark,
                radius: 20,
                child: Icon(Icons.clear, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
