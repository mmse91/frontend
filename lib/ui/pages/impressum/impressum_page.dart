import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/common/app_colors.dart';

class Impressum extends StatelessWidget {
  final String i =
      "\nTHM Gießen,\nWiesenstraße 14,\nD - 35390 Gießen,\nTelefon: +49 (641) 309 2337,\nE-Mail: FeedbackApp@protonmail.com";
  final String i2 =
      "Die Technische Hochschule Mittelhessen, insbesondere das Institut für Mathematik, Naturwissenschaften und Informatik, "
      "verarbeitet Ihre personenbezogenen Daten ausschließlich für die Durchführung von Quiz bzw. Umfragen sowie zur Erstellung von Statistiken. "
      "Die Statisten enthalten die Daten über die Antworten auf Fragen innerhalb eines Quiz oder einer Umfrage bzw. über alle Quiz in einem Kurs. "
      "Die Statistiken können nur von Ihnen selbst und den Lehrenden/Tutor*innen des Quiz oder des Kurses eingesehen werden. "
      "Rechtsgrundlage ist Art. 6 der Europäischen Datenschutzgrundverordnung. Ihre Daten bleiben gespeichert, so lange dies für die Erfüllung der genannten Zwecke erforderlich ist.\n";
  final String i3 =
      "Sie sind jederzeit berechtigt, über Ihre Daten Auskunft zu verlangen und unrichtige Daten berichtigen oder ihre Verarbeitung einschränken zu "
      "lassen (Art. 15, 16 und 18 der Europäischen Datenschutzgrundverordnung). Falls Sie die Datenverarbeitung für rechtswidrig halten, können Sie Beschwerde"
      " beim Hessischen Datenschutzbeauftragten erheben (Art. 77 der Europäischen Datenschutzgrundverordnung, § 55 des Hess. Datenschutz- und Informationsfreiheitsgesetzes). "
      "Darüber hinaus sind Sie berechtigt, Ihre Einwilligung in die Datenverarbeitung zu widerrufen und die Löschung Ihrer Daten zu verlangen, wobei die bis dahin erfolgte"
      " Datenverarbeitung rechtmäßig bleibt (Art. 7 Abs. 3 und Art. 17. EU-DSGVO).\n\n";
  final String i4 =
      "Der Betreiber fühlt sich dem Schutz der Privatsphäre seiner Kunden bei der Verarbeitung personenbezogenen Daten in hohem Maße verpflichtet und berücksichtigt dies "
      "bei allen Geschäftsprozessen. Der Betreiber verpflichtet sich, die einschlägigen Datenschutzvorgaben zu beachten. "
      "Diese Datenschutzerklärung gilt für die gesamte Webseite, nicht aber für Seiten anderer Anbieter, auf die diese Seite verlinkt.\n\n Grundsätzlich erfährt, "
      "erfasst und nutzt das System nur die Daten, die Sie uns bei der Nutzung unserer Webseite im Rahmen Ihrer Registrierung bzw. Anmeldung mitteilen. Diese Daten "
      "werden vertraulich behandelt. Ihnen ist bekannt und sie willigen darin ein, dass Ihre personenbezogenen Daten für die Funktionalität dieses System erhoben, genutzt "
      "und gespeichert werden. Personenbezogene Daten sind solche, die Angaben über persönliche und sachliche Verhältnisse des Benutzers enthalten.\n Zur Beantwortung Ihrer Fragen,"
      " Bearbeitung Ihrer Anforderungen oder Ihrer Betreuung ist es manchmal erforderlich, Sie nach persönlichen Daten wie Name, und / oder E-Mail-Adresse zu fragen. "
      "Der Betreiber kann diese Daten zur Beantwortung Ihrer Anfragen oder für die Kontaktaufnahme mit Ihnen per E-Mail oder Telefon verwenden.\n  Der Betreiber darf Sie auf "
      "diesem Wege auch über neue Funktionalitäten des Feedbacksystem informieren.\n  Der Betreiber kann auf eigene Initiative oder auf Ihren Wunsch hin unvollständige, "
      "fehlerhafte oder veraltete persönliche Daten vervollständigen, berichtigen oder löschen, die von dem Feedbacksystem im Zusammenhang mit dem Betrieb dieser Seite gespeichert werden.\n"
      "  Der Betreiber löscht im Rahmen der gesetzlichen Bestimmungen personenbezogene Daten unverzüglich auf Veranlassung des Berechtigten, sofern dem nicht "
      "zwingende gesetzliche Aufbewahrungspflichten entgegenstehen.";
  final String i5 =
      "Ihre personenbezogenen Daten werden von uns über das Internet unter Anwendung des sog. SSL (Secure-Sockets-Layer)-Verfahrens "
      "verschlüsselt übertragen. Dennoch ist die Bereitstellung persönlicher Daten, ob dies persönlich, telefonisch oder über das Internet geschieht, stets mit Risiken "
      "verbunden und kein technologisches System ist völlig unanfällig für Manipulationen oder Sabotage. Der Betreiber hat versucht, in zumutbarem Umfang Vorkehrungen zu"
      " treffen, um unbefugten Zugriff auf Ihre per­sönlichen Daten sowie die unbefugte Verwendung oder Verfälschung dieser Daten zu verhindern und die entsprechenden Risiken zu minimieren.";

  final String i6 =
      "Er werden lediglich JWT-Tokens gespeichert. Wird anonym an einem Quiz teilgenommen wird für die Dauer "
      "des Quiz ein Sitzungstoken gespeichert, um am Ende des Quiz ausgeben zu können wie viele Fragen korrekt beantwortet wurden. Wird eine Anmeldung "
      "mit der THM-Benutzerdaten durchgeführt wird bis zur (automatischen) Abmeldung ein Authentifizierungstoken gespeichert.";

  final String i7 =
      "Gelegentlich enthält die Lernplattform interaktive Verweise (sog. Links) "
      "auf Internetauftritte Dritter, für die wir nicht verantwortlich sind. Insbesondere haben wir keinerlei Einfluss auf Inhalt und "
      "Gestaltung der verlinkten externen Seiten bzw. den Internetauftritten, auf die Sie über diese Seiten gelangen. Für Inhalt und Gestaltung dieser "
      "Internetauftritte sowie die Einhaltung datenschutzrechtlicher Bestimmungen sind ausschließlich die jeweiligen Anbieter verantwortlich.";

  final String i8 =
      "Wir behalten uns das Recht vor, diese Bestimmungen jederzeit mit Wirkung für die Zukunft zu ändern.";

  final String i9 =
      "Sie können sich jederzeit an uns wenden, wenn Sie wünschen, dass Ihr "
      "Profil und die zu Ihrer Person gespeicherten Daten berichtigt, gesperrt oder gelöscht werden. Darüber hinaus erteilen wir Ihnen "
      "jederzeit Auskunft darüber, welche Daten wir von Ihnen gespeichert haben sowie deren Herkunft und Empfänger sowie den Zweck der Speicherung."
      " Für Fragen zum Datenschutz wenden Sie sich bitte an:";

  final String i10 =
      "Impressum \n THM Gießen, \nWiesenstraße 14,\nD - 35390 Gießen,\nTelefon: +49 (641) 309 2337,\nE-Mail: FeedbackApp@protonmail.com";

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: FBSAppBar(
          title: 'Impressum',
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Container(
                    constraints: ((MediaQuery.of(context).size.width) > 768)
                        ? BoxConstraints(minWidth: 100, maxWidth: 1024)
                        : null,
                    padding: EdgeInsets.all(35),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                          child: Text(
                            "Impressum",
                            style: TextStyle(
                              fontSize: 20,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                          child: Text(
                            "1. Allgemeine Hinweise",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 10),
                          child: Text(
                            "2. Erfassung und Nutzung Ihrer personenbezogenen Informationen zum Datenschutz",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i2 + i3 + i4,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 10),
                          child: Text(
                            "3. Schutz Ihrer Daten",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i5,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 10),
                          child: Text(
                            "4. Einsatz von Cookies",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i6,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 10),
                          child: Text(
                            "5. Links zu anderen Webseiten",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i7,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 10),
                          child: Text(
                            "6. Änderungen",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i8,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 10),
                          child: Text(
                            "7. Fragen zum Datenschutz und Kontakt",
                            style: TextStyle(
                              fontSize: 18,
                              color: font_dark,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          i9,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                        Text(
                          i,
                          style: TextStyle(
                            fontSize: 16,
                            color: font_dark,
                          ),
                        ),
                      ],
                    )))),
      ),
    );
  }
}
