import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:pie_chart/pie_chart.dart';

class MeProfilePage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final Session session;

  MeProfilePage({this.session});

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => UserTotalStatisticsService(api: Injector.get()))],
        builder: (context) {
          return Scaffold(
            appBar: FBSAppBar(
              title: "Mein Profil",
            ),
            body: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  constraints:
                      BoxConstraints(maxWidth: 850), // limit the width for Web
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                    child: SingleChildScrollView(
                        child: Center(
                      child: Column(
                        children: <Widget>[
                          UIHelper.verticalSpaceMedium(),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Informationen',
                              style: headline4,
                            ),
                          ),
                          UIHelper.verticalSpaceSmall(),
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 3,
                                  blurRadius: 5,
                                  offset: Offset(0, 5),
                                ),
                              ],
                            ),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text("Username:", style: headline5),
                                        Text("Nachname:", style: headline5),
                                        Text("Vorname:", style: headline5),
                                        Text("Rolle:", style: headline5),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text(user.username,
                                            style: TextStyle(color: font_dark)),
                                        Text(user.lastname,
                                            style: TextStyle(color: font_dark)),
                                        Text(user.firstname,
                                            style: TextStyle(color: font_dark)),
                                        Text(
                                            user.docent == true
                                                ? 'Dozent'
                                                : 'Student',
                                            style: TextStyle(color: font_dark)),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          UIHelper.verticalSpaceMedium(),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Statistiken',
                              textAlign: TextAlign.start,
                              style: headline4,
                            ),
                          ),
                          UIHelper.verticalSpaceSmall(),
                          WhenRebuilderOr<UserTotalStatisticsService>(
                            observe: () => RM.get<UserTotalStatisticsService>(),
                            initState: (_, userTotalStatisticsServiceRM) {
                              userTotalStatisticsServiceRM.setState(
                                  (state) => state.getUserStats(user.authToken),
                                  onError: (context, e) {
                                ErrorHandler.showErrorDialog(context, e);
                              });
                            },
                            onWaiting: () {
                              return Container();
                            },
                            onError: (error) => Center(
                                child: Text(ErrorHandler.errorMessage(error),
                                    style: headline5)),
                            builder: (_, userTotalStatisticsService) {
                              return Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.1),
                                      spreadRadius: 3,
                                      blurRadius: 5,
                                      offset: Offset(
                                          0, 5), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Beantwortete Fragen',
                                        style: headline5,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    UIHelper.verticalSpaceSmall(),
                                    userTotalStatisticsService
                                                .state.stats.answQuestions >
                                            0
                                        ? PieChart(
                                            dataMap: userTotalStatisticsService
                                                .state.stats.chartData,
                                            colorList: [
                                              THMGreen,
                                              Colors.red[300]
                                            ],
                                            legendStyle: TextStyle(
                                                color: font_dark,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                            showChartValuesInPercentage: true,
                                            legendPosition:
                                                LegendPosition.right,
                                          )
                                        : Text(
                                            "Es wurde noch keine Session abgeschlossen."),
                                    UIHelper.verticalSpaceSmall(),
                                    Column(
                                      children: [
                                        RichText(
                                          text: TextSpan(
                                            text: 'Eingeschriebene Kurse: ',
                                            style: headline5,
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      userTotalStatisticsService
                                                          .state.stats.courses
                                                          .toString(),
                                                  style: null),
                                            ],
                                          ),
                                        ),
                                        RichText(
                                          text: TextSpan(
                                            text: 'Bearbeitete Sessions: ',
                                            style: headline5,
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      userTotalStatisticsService
                                                          .state.stats.sessions
                                                          .toString(),
                                                  style: null),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                          UIHelper.verticalSpaceMedium(),
                        ],
                      ),
                    )),
                  ),
                )),
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 2)
                : null,
          );
        });
  }
}
