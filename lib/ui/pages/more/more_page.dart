import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:fbs_app/ui/widgets/main_app_bg.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class MorePage extends StatelessWidget {
  final TextEditingController usernameController = new TextEditingController();
  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    var radius = ((MediaQuery.of(context).size.width) > 768)
        ? 153.6
        : ((MediaQuery.of(context).size.width) * 0.2);
    var initials = user.firstname == null || user.lastname == null
        ? ""
        : user.firstname[0] + user.lastname[0];
    return Scaffold(
      body: SingleChildScrollView(
          child: Stack(children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: MainAppBackground(
              headerHeight: (MediaQuery.of(context).size.height) / 4),
        ),
        SafeArea(
          child: Center(
            child: Container(
              constraints: ((MediaQuery.of(context).size.width) > 768)
                  ? BoxConstraints(minWidth: 100, maxWidth: 500)
                  : null,
              child: Column(children: [
                Container(
                    padding: const EdgeInsets.only(top: 30),
                    child: Center(
                      child: CircleAvatar(
                          radius: radius,
                          backgroundColor: white,
                          child: Center(
                            child: Text(
                              initials,
                              maxLines: 1,
                              overflow: TextOverflow.visible,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: radius * 0.7,
                                color: green,
                              ),
                            ),
                          )),
                    )),
                Container(
                  padding: EdgeInsets.fromLTRB(25, 25, 25, 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Text(
                          'Mehr Funktionalitäten',
                          textAlign: TextAlign.start,
                          style: headline4,
                        ),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.pushNamed(context, 'me/profile'),
                        child: FbsListCard(
                            heading: 'Mein Profil',
                            subHeading: 'Profil anzeigen',
                            icon: Icon(Icons.person, color: greyLetters)),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.pushNamed(context, 'me/quizzes'),
                        child: FbsListCard(
                            heading: 'Meine Quizze',
                            subHeading: 'Eigene Quizze verwalten',
                            icon: Icon(Icons.question_answer,
                                color: greyLetters)),
                      ),
                      user.docent == true
                          ? GestureDetector(
                              onTap: () =>
                                  Navigator.pushNamed(context, 'me/courses'),
                              child: FbsListCard(
                                  heading: 'Meine Kurse',
                                  subHeading: 'Eigene Kurse verwalten',
                                  icon: Icon(Icons.school, color: greyLetters)),
                            )
                          : Container(),
                      GestureDetector(
                        onTap: () =>
                            Navigator.pushNamed(context, 'me/sessions'),
                        child: FbsListCard(
                            heading: 'Meine Sessions',
                            subHeading: 'Eigene Sessions verwalten',
                            icon: Icon(Icons.schedule, color: greyLetters)),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.pushNamed(context, 'contact'),
                        child: FbsListCard(
                            heading: 'Kontakt',
                            subHeading:
                            'Fragen stellen oder Fehlverhalten melden',
                            icon: Icon(Icons.mail, color: greyLetters)),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ),
        ),
      ])),
      bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
          ? FbsBottomNavbar(index: 2)
          : null,
    );
  }
}
