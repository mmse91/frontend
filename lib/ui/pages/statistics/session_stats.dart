import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/photo_layouts.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class SessionStatsPage extends StatefulWidget {
  SessionStatsPage({Key key, @required this.session}) : super(key: key);

  final Session session;

  @override
  _SessionStats createState() => _SessionStats(session);
}

class _SessionStats extends State<SessionStatsPage> {
  final Session session;

  final user = Injector.get<AuthenticationService>().user;

  _SessionStats(this.session);

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: FBSAppBar(title: 'Gesamtübersicht'),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Center(
            child: Container(
              padding: EdgeInsets.fromLTRB(24, 0, 24, 0),
              constraints: ((MediaQuery.of(context).size.width) > 768)
                  ? BoxConstraints(minWidth: 100, maxWidth: 500)
                  : null,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    UIHelper.verticalSpaceMedium(),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Informationen',
                        style: headline4,
                      ),
                    ),
                    UIHelper.verticalSpaceSmall(),
                    Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 3,
                            blurRadius: 5,
                            offset: Offset(0, 5),
                          ),
                        ],
                      ),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Name des Quizzes",
                            style: headline5,
                          ),
                          Text(session.quiz.name,
                              style: TextStyle(color: font_dark)),
                          SizedBox(height: 10),
                          Text("Dauer der Session:", style: headline5),
                          Text('${session.startDate} bis ${session.endDate}',
                              style: TextStyle(color: font_dark)),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Anzahl der Fragen:", style: headline5),
                              Text(session.quiz.questionCount.toString(),
                                  style: TextStyle(color: font_dark)),
                            ],
                          ),
                        ],
                      ),
                    ),
                    UIHelper.verticalSpaceMedium(),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Statistiken',
                        style: headline4,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(25),
                      constraints: ((MediaQuery.of(context).size.width) > 768)
                          ? BoxConstraints(minWidth: 100, maxWidth: 500)
                          : null,
                      child: Text(
                        "Die folgende Statistik zeigt die Anzahl der richtig beantworteten Fragen dieser Session im Vergleich zu den insgesamt abgegebenen Antworten.",
                        style: TextStyle(color: font_dark),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: WhenRebuilderOr<SessionStatisticsService>(
                        observe: () => RM.get<SessionStatisticsService>(),
                        onError: (error) {
                          return Container(
                            child: Center(
                              child: Text(ErrorHandler.errorMessage(error),
                                  textAlign: TextAlign.center),
                            ),
                          );
                        },
                        initState: (_, statServiceRM) {
                          statServiceRM.setState(
                              (state) =>
                                  state.sessStats(session, user.authToken),
                              onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          });
                        },
                        onWaiting: () {
                          return Container(
                            child: Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation<Color>(THMGreen),
                              ),
                            ),
                          );
                        },
                        builder: (_, statServiceRM) {
                          return statServiceRM.state.sessionImage != null
                              ? PhotoLayout(statServiceRM.state.sessionImage)
                              : Container(
                                  child: Center(
                                      child: Text(
                                    "Es konnte keine Statistik erstellt werden",
                                    style: headline3,
                                    textAlign: TextAlign.center,
                                  )),
                                );
                        },
                      ),
                    ),
                    UIHelper.verticalSpaceMedium(),
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
