import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';

import 'questionStats_widget.dart';
import 'package:swipedetector/swipedetector.dart';

class StatisticsQuestionsDetailPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final questionStats = Injector.get<SessionStatisticsService>().questionStats;
  final int index;

  StatisticsQuestionsDetailPage({this.index});

  createEndOfQuestionsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Ende Gelände'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                    "Du hast die letzte Frage des Quiz erreicht. Willst du zur Übersicht zurück?"),
                UIHelper.verticalSpaceSmall(),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Nein!', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('Ja, bitte!', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    QuestionStatistic questionStat = questionStats[index];
    return Scaffold(
        appBar: FBSAppBar(
          title: "Frage ${questionStat.question.number.toString()}",
        ),
        body: SwipeDetector(
          swipeConfiguration: SwipeConfiguration(
            horizontalSwipeMinVelocity: 50,
          ),
          onSwipeLeft: () {
            if (index < questionStats.length - 1)
              Navigator.pushReplacement(
                  context,
                  PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: StatisticsQuestionsDetailPage(
                        index: index + 1,
                      )));
            else
              createEndOfQuestionsDialog(context);
          },
          onSwipeRight: () {
            if (index > 0)
              Navigator.pushReplacement(
                  context,
                  PageTransition(
                      type: PageTransitionType.leftToRight,
                      child: StatisticsQuestionsDetailPage(
                        index: index - 1,
                      )));
            else
              Navigator.of(context).pop();
          },
          child: Align(
            alignment: Alignment.topCenter,
            child: Container(
              constraints:
                  BoxConstraints(maxWidth: 850), // limit the width for Web
              child: Padding(
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      UIHelper.verticalSpaceMedium(),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Informationen',
                          style: headline4,
                        ),
                      ),
                      UIHelper.verticalSpaceSmall(),
                      Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.1),
                              spreadRadius: 3,
                              blurRadius: 5,
                              offset: Offset(0, 5),
                            ),
                          ],
                        ),
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Fragestellung:",
                              style: headline5,
                            ),
                            Text(questionStat.question.question,
                                style: TextStyle(color: font_dark)),
                            SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Fragetyp:", style: headline5),
                                Text(questionStat.type,
                                    style: TextStyle(color: font_dark)),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Beantwortungen:", style: headline5),
                                Text(questionStat.answered.toString(),
                                    style: TextStyle(color: font_dark)),
                              ],
                            ),
                          ],
                        ),
                      ),
                      UIHelper.verticalSpaceMedium(),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Statistiken',
                              textAlign: TextAlign.start,
                              style: headline4,
                            ),
                          ),
                          UIHelper.verticalSpaceSmall(),
                          // Platz für Widget
                          Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 3,
                                    blurRadius: 5,
                                    offset: Offset(0, 5),
                                  ),
                                ],
                              ),
                              alignment: Alignment.centerLeft,
                              child: QuestionStatisticWidget(questionStat))
                        ],
                      ),
                      UIHelper.verticalSpaceMedium(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
            ? FbsBottomNavbar(index: 2)
            : null);
  }
}
