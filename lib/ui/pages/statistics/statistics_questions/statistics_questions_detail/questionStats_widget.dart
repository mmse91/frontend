import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/photo_layouts.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class QuestionStatisticWidget extends StatelessWidget {
  final QuestionStatistic stat;

  QuestionStatisticWidget(this.stat);

  @override
  Widget build(BuildContext context) {
    return Container(child: _getWidget());
  }

  Widget _getWidget() {
    if (stat is MultipleChoiceStatistic) {
      return MultipleChoice(stat);
    } else if (stat is OpenStatistic) {
      return Open(stat);
    } else if (stat is SortStatistic) {
      return Sort(stat);
    } else if (stat is WordCloudStatistic) {
      return WordCloud(stat);
    } else if (stat is RatingStatistic) {
      return Rating(stat);
    } else {
      throw UnimplementedError(
          'Other question types are yet unimplemented.'); // <- Programmierfehler
    }
  }
}

class MultipleChoice extends StatelessWidget {
  final MultipleChoiceStatistic _mcStat;

  const MultipleChoice(this._mcStat);

  @override
  Widget build(BuildContext context) {
    return _mcStat.stats.isNotEmpty
        ? PieChart(
            chartRadius: MediaQuery.of(context).size.width * 0.5 > 300
                ? 300
                : MediaQuery.of(context).size.width * 0.5,
            dataMap: _mcStat.stats,
            colorList: [THMGreen, bg_green, yellow, blue],
            legendStyle: TextStyle(
                color: font_dark, fontWeight: FontWeight.bold, fontSize: 12),
            showChartValuesInPercentage: false,
            legendPosition: ((MediaQuery.of(context).size.width) < 768)
                ? LegendPosition.bottom
                : LegendPosition.right,
          )
        : Text('Es existieren keine Daten.',
            style: TextStyle(color: font_dark));
  }
}

class Open extends StatelessWidget {
  final OpenStatistic _openStat;

  const Open(this._openStat);

  @override
  Widget build(BuildContext context) {
    var length = _openStat.stats.length;
    return _openStat.stats.isNotEmpty
        ? Container(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: length,
                itemBuilder: (_, index) {
                  return Column(children: [
                    Text(
                      _openStat.stats[index],
                      textAlign: TextAlign.center,
                      style: TextStyle(color: font_dark),
                    ),
                    index == length - 1
                        ? Container()
                        : Divider(
                            color: Colors.grey,
                            thickness: 1,
                          )
                  ]);
                }),
          )
        : Text('Es existieren keine Daten.',
            style: TextStyle(color: font_dark));
  }
}

class WordCloud extends StatelessWidget {
  final WordCloudStatistic _wcStat;
  final user = Injector.get<AuthenticationService>().user;

  WordCloud(this._wcStat);

  @override
  Widget build(BuildContext context) {
    return WhenRebuilderOr<SessionStatisticsService>(
      observe: () => RM.get<SessionStatisticsService>(),
      initState: (_, statServiceRM) {
        statServiceRM.setState((state) =>
            state.getWordCloudImage(_wcStat.question.number, user.authToken));
      },
      onWaiting: () =>
          Container(
        child: Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
          ),
        ),
          ),
      onError: (error) {
        return Container(
          child: Center(
            child: Text(ErrorHandler.errorMessage(error),
                textAlign: TextAlign.center),
          ),
        );
      },
      builder: (_, statServiceRM) {
        return statServiceRM.state.wordCloudImage != null
            ? PhotoLayout(statServiceRM.state.wordCloudImage, card: false)
            : Container(
          padding: EdgeInsets.all(25),
          child: Center(
              child: Text(
                "Es konnte keine WordCloud erstellt werden",
                style: headline3,
                textAlign: TextAlign.center,
              )),
        );
      },
    );
  }
}

class Rating extends StatelessWidget {
  final RatingStatistic _ratingStat;

  const Rating(this._ratingStat);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Column(children: [
        _ratingStat.stats.isNotEmpty
            ? RichText(
                text: TextSpan(
                  text: 'Mittelwert: ',
                  style: headline5,
                  children: <TextSpan>[
                    TextSpan(
                        text: _ratingStat.mean.toStringAsFixed(2), style: null),
                  ],
                ),
              )
            : Container(),
      ]),
      UIHelper.verticalSpaceSmall(),
      _ratingStat.stats.isNotEmpty
          ? PieChart(
              chartRadius: MediaQuery.of(context).size.width * 0.5 > 300
                  ? 300
                  : MediaQuery.of(context).size.width * 0.5,
              dataMap: _ratingStat.stats,
              colorList: [
                THMGreen,
                bg_green,
                yellow,
                Colors.red[300],
                darkRedHighlight
              ],
              legendStyle: TextStyle(
                  color: font_dark, fontWeight: FontWeight.bold, fontSize: 12),
              showChartValuesInPercentage: false,
        legendPosition: ((MediaQuery
            .of(context)
            .size
            .width) < 768)
            ? LegendPosition.bottom
            : LegendPosition.right,
            )
          : Text('Es existieren keine Daten.',
              style: TextStyle(color: font_dark))
    ]);
  }
}

class Sort extends StatelessWidget {
  final SortStatistic _sortStat;

  const Sort(this._sortStat);

  @override
  Widget build(BuildContext context) {
    return _sortStat.stats.isNotEmpty
        ? PieChart(
            chartRadius: MediaQuery.of(context).size.width * 0.5 > 300
                ? 300
                : MediaQuery.of(context).size.width * 0.5,
            dataMap: _sortStat.stats,
            colorList: [THMGreen, darkRedHighlight],
            legendStyle: TextStyle(
                color: font_dark, fontWeight: FontWeight.bold, fontSize: 12),
            showChartValuesInPercentage: false,
      legendPosition: ((MediaQuery
          .of(context)
          .size
          .width) < 768)
          ? LegendPosition.bottom
          : LegendPosition.right,
          )
        : Text('Es existieren keine Daten.',
            style: TextStyle(color: font_dark));
  }
}
