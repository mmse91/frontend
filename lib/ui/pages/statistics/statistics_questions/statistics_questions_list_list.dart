import 'package:fbs_app/domain/entities/question/questionStatistics.dart';
import 'package:fbs_app/ui/pages/statistics/statistics_questions/statistics_questions_list_item.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/ui/common/text_styles.dart';

class StatisticsQuestionsList extends StatelessWidget {
  StatisticsQuestionsList({this.questionStats});

  final List<QuestionStatistic> questionStats;

  @override
  Widget build(BuildContext context) {
    return questionStats.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: questionStats.length,
              itemBuilder: (context, index) => StatisticsQuestionsListItem(
                questionStat: questionStats[index],
                onTap: () {
                  Navigator.pushNamed(context, 'statistics/questions/detail',
                      arguments: index);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Keine Statistiken gefunden",
                style: headline3,
                textAlign: TextAlign.center,
              ),
            ],
          )));
  }
}
