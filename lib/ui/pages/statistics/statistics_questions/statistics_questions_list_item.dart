import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

class StatisticsQuestionsListItem extends StatelessWidget {
  final QuestionStatistic questionStat;
  final Function onTap;

  const StatisticsQuestionsListItem({this.onTap, this.questionStat});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: "${questionStat.question.question}",
          subHeading: "${questionStat.type}",
          customIconText: questionStat.question.number.toString(),
        ));
  }
}
