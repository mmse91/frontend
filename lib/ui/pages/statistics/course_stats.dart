import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/widgets/photo_layouts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class CourseStatsPage extends StatefulWidget {
  CourseStatsPage({Key key, @required this.course}) : super(key: key);

  final Course course;

  @override
  _CourseStats createState() => _CourseStats(course);
}

class _CourseStats extends State<CourseStatsPage> {
  Course course;

  final user = Injector.get<AuthenticationService>().user;

  final sessionsNum = Injector.get<CoursesService>().sessions.length;

  _CourseStats(this.course);

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: FBSAppBar(title: course.name),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Center(
              child: Container(
                padding: EdgeInsets.fromLTRB(24, 0, 24, 0),
                constraints: ((MediaQuery.of(context).size.width) > 768)
                    ? BoxConstraints(minWidth: 100, maxWidth: 500)
                    : null,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      UIHelper.verticalSpaceMedium(),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Informationen',
                          style: headline4,
                        ),
                      ),
                      UIHelper.verticalSpaceSmall(),
                      WhenRebuilderOr(
                          observe: () => RM.get<CoursesService>(),
                          initState: (_, courseServiceRM) {
                            courseServiceRM.setState((state) =>
                                state.getCourseParticipants(
                                    course.id, user.authToken));
                          },
                          onWaiting: () {
                            return Container(
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      THMGreen),
                                ),
                              ),
                            );
                          },
                          builder: (_, courseServiceRM) {
                            return Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 3,
                                    blurRadius: 5,
                                    offset: Offset(0, 5),
                                  ),
                                ],
                              ),
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Aktive Sessions:",
                                          style: headline5),
                                      Text(sessionsNum.toString(),
                                          style: TextStyle(color: font_dark)),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Teilnehmer:", style: headline5),
                                      Text(
                                          courseServiceRM
                                              .state.participants.length
                                              .toString(),
                                          style: TextStyle(color: font_dark)),
                                    ],
                                  ),
                                ],
                              ),
                            );
                          }),
                      UIHelper.verticalSpaceMedium(),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Statistiken',
                          style: headline4,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(25),
                        constraints: ((MediaQuery.of(context).size.width) > 768)
                            ? BoxConstraints(minWidth: 100, maxWidth: 500)
                            : null,
                        child: Text(
                          "Die folgende Statistik zeigt die vollständig richtig beantworteten Quiz-Sessions dieses Kurses im Vergleich zu den insgesamt abgeschlossenen Quiz-Sessions.",
                          style: TextStyle(color: bg_dark),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        child: sessionsNum > 0
                            ? WhenRebuilderOr<SessionStatisticsService>(
                                observe: () =>
                                    RM.get<SessionStatisticsService>(),
                                onError: (error) {
                                  return Container(
                                    child: Center(
                                      child: Text(
                                          ErrorHandler.errorMessage(error),
                                          textAlign: TextAlign.center),
                                    ),
                                  );
                                },
                                initState: (_, statServiceRM) {
                                  statServiceRM.setState(
                                      (state) => state.courseStats(
                                          course, user.authToken),
                                      onError: (context, e) {
                                    ErrorHandler.showErrorDialog(context, e);
                                  });
                                },
                                onWaiting: () {
                                  return Container(
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                THMGreen),
                                      ),
                                    ),
                                  );
                                },
                                builder: (_, statServiceRM) {
                                  return statServiceRM.state.courseImage != null
                                      ? PhotoLayout(
                                          statServiceRM.state.courseImage)
                                      : Container(
                                          child: Center(
                                              child: Text(
                                            "Es konnte keine Statistik erstellt werden",
                                            style: headline3,
                                            textAlign: TextAlign.center,
                                          )),
                                        );
                                },
                              )
                            : Container(
                                padding: EdgeInsets.all(25),
                                child: Center(
                                    child: Text(
                                  "Es konnte keine Statistik erstellt werden",
                                  style: headline3,
                                  textAlign: TextAlign.center,
                                )),
                              ),
                      ),
                      UIHelper.verticalSpaceMedium(),
                    ]),
              ),
            ),
          ),
        ),
        bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
            ? FbsBottomNavbar(index: 2)
            : null);
  }
}
