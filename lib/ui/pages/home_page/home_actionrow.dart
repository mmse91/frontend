import 'package:fbs_app/domain/entities/user.dart';
import 'package:fbs_app/service/create_quiz_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class HomeActionRow extends StatelessWidget {
  HomeActionRow(
      {this.action1Name,
      this.action1Route,
      this.action2Name,
      this.action2Route,
      this.user});

  final String action1Name;
  final String action1Route;
  final String action2Name;
  final String action2Route;
  final User user;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: InkWell(
            onTap: () {
              Injector.get<CreateQuizService>().comesFromHome = true;
              Navigator.pushNamed(context, 'quiz/newquiz');
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: Offset(0, 5), // changes position of shadow
                  ),
                ],
              ),
              width: 150,
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(
                        Icons.question_answer,
                        size: 80,
                        color: Color(0XFF394A59),
                      ),
                      Text('Quiz erstellen',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0XFF394A59))),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: 30),
        Expanded(
          child: InkWell(
            onTap: () {
              showAlertDialog(context);
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: Offset(0, 5), // changes position of shadow
                  ),
                ],
              ),
              width: 150,
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(
                        Icons.fast_forward,
                        size: 80,
                        color: Color(0XFF394A59),
                      ),
                      Text('An Quiz teilnehmen',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0XFF394A59))),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  showAlertDialog(BuildContext context) {
    final TextEditingController sessionIdController =
        new TextEditingController();
    Widget cancel = FlatButton(
      child: Text("Abbrechen", style: TextStyle(color: font_dark)),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget ok = WhenRebuilderOr<SessionService>(
      observe: () => RM.get<SessionService>(),
      onWaiting: () => Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(green),
        ),
      ),
      dispose: (_, __) {
        sessionIdController.dispose();
      },
      builder: (_, sessServiceRM) {
        return Center(
          child: FlatButton(
            child: Text("OK", style: TextStyle(color: font_dark)),
            onPressed: () {
              sessServiceRM.setState(
                  (state) => state.getSession(sessionIdController.text),
                  onData: (context, sessServiceRM) {
                if (sessServiceRM.session != null) {
                  Navigator.pushReplacementNamed(context, 'quiz/begin',
                      arguments: sessServiceRM.session);
                }
              }, onError: (context, e) {
                ErrorHandler.showErrorDialog(context, e);
              });
            },
          ),
        );
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))),
      title: Text("Session-Id eingeben", style: TextStyle(color: font_dark)),
      content: TextField(
        keyboardType: TextInputType.number,
        controller: sessionIdController,
        decoration: InputDecoration(
          hintText: 'Beispiel: 12345678',
          border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(30.0),
              borderSide: new BorderSide()),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: green, width: 2.0),
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
      ),
      actions: [
        cancel,
        ok,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
