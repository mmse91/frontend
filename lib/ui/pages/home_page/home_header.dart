import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class HomeHeader extends StatelessWidget {
  HomeHeader({this.name, this.isTeacher = true});

  final String name;
  final bool isTeacher;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Willkommen, \n$name',
            textAlign: TextAlign.left, style: headline2),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            InkWell(
              onTap: () {
                Injector.get<AuthenticationService>().logout().catchError(
                    (error) => ErrorHandler.showErrorDialog(context, error));
                Navigator.of(context).pushNamedAndRemoveUntil(
                    'login', (Route<dynamic> route) => false); //pop all routes!
              },
              child: CircleAvatar(
                backgroundColor: bg_dark,
                radius: 30,
                child: Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
