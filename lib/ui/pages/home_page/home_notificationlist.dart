import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

class HomeNotificationList extends StatelessWidget {
  HomeNotificationList({this.heading = "Aktuelle Kurse"});

  final String heading;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(bottom: 10),
            alignment: Alignment.topLeft,
            child: Text(
              'Aktuelle Kurse',
              textAlign: TextAlign.center,
              style: headline4,
            )),
        FbsListCard(
          heading: "Datenbanksysteme",
          subHeading: "7 neue Abgaben",
        ),
        FbsListCard(
          heading: "Datenbanksysteme",
          subHeading: "7 neue Abgaben",
        )
      ],
    );
  }
}
