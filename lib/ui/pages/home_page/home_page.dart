import 'package:fbs_app/ui/pages/home_page/home_actionrow.dart';
import 'package:fbs_app/ui/pages/home_page/home_join_quiz.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/pages/home_page/home_header.dart';
import 'package:fbs_app/ui/pages/home_page/home_recentlist.dart';
import 'package:fbs_app/ui/widgets/main_app_bg.dart';
import 'package:fbs_app/ui/widgets/webNavigationDrawer.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import '../../../service/authentication_service.dart';
import '../../../service/courses_service.dart';
import '../../exceptions/error_handler.dart';

class HomePage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;

  Widget content(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: MediaQuery.of(context).size.width > 768
            ? //mobile: appbar
            AppBar(
                title: Text(
                  "Home",
                  style: TextStyle(color: white, fontSize: 20),
                ),
                automaticallyImplyLeading: false,
                backgroundColor: bg_dark,
                actions: [
                  InkWell(
                      onTap: () {
                        Injector.get<AuthenticationService>()
                            .logout()
                            .catchError((error) =>
                                ErrorHandler.showErrorDialog(context, error));
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            'login', (Route<dynamic> route) => false);
                      },
                      child: Row(
                        children: [
                          Text("Ausloggen",
                              style: TextStyle(color: white, fontSize: 18)),
                          CircleAvatar(
                            backgroundColor: bg_dark,
                            radius: 30,
                            child: Icon(
                              Icons.exit_to_app,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ))
                ],
              )
            : null,
        body: WhenRebuilderOr<CoursesService>(
          observe: () => RM.get<CoursesService>(),
          initState: (_, coursesServiceRM) {
            coursesServiceRM.setState(
                (state) => state.getUndoneSessionsForUser(user.authToken),
                onError: (context, e) {
              ErrorHandler.showErrorDialog(context, e);
            });
          },
          onWaiting: () => Center(child: CircularProgressIndicator()),
          builder: (_, coursesService) {
            return Stack(
              children: [
                MainAppBackground(
                    headerHeight: (MediaQuery.of(context).size.height) / 4),
                SafeArea(
                  child: SingleChildScrollView(
                    child: Container(
                        // SingleChildScrollView needed?
                        padding: MediaQuery.of(context).size.width < 768
                            ? EdgeInsets.all(25)
                            : EdgeInsets.all(50),
                        constraints: BoxConstraints(maxWidth: 850),
                        child: MediaQuery.of(context).size.width < 768
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  HomeHeader(name: user.username),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  (!user.docent)
                                      ? HomeJoinQuiz(user.authToken)
                                      : HomeActionRow(user: user),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  HomeRecentList(
                                      sessions:
                                          coursesService.state.undoneSessions),
                                ],
                              )
                            : Container(
                                height: MediaQuery.of(context).size.height,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Willkommen, " + user.username,
                                      style: TextStyle(
                                        fontSize: 40,
                                        fontWeight: FontWeight.bold,
                                        color: font_dark,
                                      ),
                                    ),
                                    Container(
                                      constraints:
                                          BoxConstraints(maxWidth: 500),
                                      margin: EdgeInsets.fromLTRB(0, 25, 0, 25),
                                      child: (!user.docent)
                                          ? HomeJoinQuiz(user.authToken)
                                          : HomeActionRow(user: user),
                                    ),
                                    HomeRecentList(
                                        sessions: coursesService
                                            .state.undoneSessions),
                                  ],
                                ),
                              )),
                  ),
                ),
              ],
            );
          },
        ),
        bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
            ? FbsBottomNavbar(index: 1)
            : null,
      ),
    );
  }

  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => CoursesService(api: Injector.get()))],
        builder: (context) {
          return MediaQuery.of(context).size.width > 768
              ? WebNavigation(content(context), "/")
              : //for web
              content(context); //for mobile
        });
  }
}
