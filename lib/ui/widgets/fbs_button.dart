import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FbsButton extends StatelessWidget {
  FbsButton(
      {@required this.onPressed,
      @required this.text,
      @required this.type,
      @required this.size});

  final GestureTapCallback onPressed;
  final String text;
  final String type;
  final String size;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: getColor(),
      splashColor: theme.primaryColorDark,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: Container(
                height: getHeight(),
                width: MediaQuery.of(context).size.width - 50,
                child: Center(
                  child: Text(
                    text,
                    maxLines: 1,
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      onPressed: onPressed,
      shape: const StadiumBorder(),
    );
  }

  double getHeight() {
    switch (size) {
      case 'big':
        return 35;
        break;
      default:
        return 20;
    }
  }


  Color getColor() {
    switch (type) {
      case 'primary':
        return theme.highlightColor;
        break;
      case 'secondary':
        return bg_dark;
        break;
      default:
        return theme.highlightColor;
    }
  }
}

