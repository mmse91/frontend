import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FbsBottomNavbar extends StatelessWidget {
  FbsBottomNavbar({@required this.index});

  final int index;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: index,
      onTap: (value) {
        String route;
        switch (value) {
          case 0:
            route = 'courses';
            break;
          case 1:
            route = 'home';
            break;
          case 2:
            route = 'more';
            break;
        }
        index == value
            ? Navigator.pushReplacementNamed(context, route)
            : Navigator.pushNamed(context, route);
      },
      backgroundColor: Colors.white,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(
            Icons.collections_bookmark,
            size: 30,
          ),
          title: Text(
            'Kurse',
            style: TextStyle(fontSize: 16),
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
            size: 30,
          ),
          title: Text(
            'Start',
            style: TextStyle(fontSize: 16),
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.more_horiz,
            size: 30,
          ),
          title: Text(
            'Mehr',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ],
      selectedItemColor: green,
    );
  }
}
