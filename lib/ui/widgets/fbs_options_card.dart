import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FbsOptionsCard extends StatelessWidget {
  FbsOptionsCard(
      {@required this.onTap, @required this.iconData, @required this.title});

  final Function onTap;
  final IconData iconData;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        highlightColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          width: 80,
          height: 130,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(
                    iconData,
                    size: 40,
                    color: Color(0XFF394A59),
                  ),
                  Text(title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: kIsWeb ? 18 : 14,
                          fontWeight: FontWeight.bold,
                          color: Color(0XFF394A59))),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
