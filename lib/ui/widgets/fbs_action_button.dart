import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class FbsActionButton extends StatelessWidget {
  FbsActionButton({@required this.type});

  final String type;

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case "menu":
        return InkWell(
          highlightColor: Colors.transparent,
          onTap: () {
            RM.get<CoursesService>().setState(
                  (state) => state.options = !state.options,
                );
          },
          child: CircleAvatar(
            backgroundColor: Colors.white,
            radius: 20,
            child: Icon(
              Icons.menu,
              color: bg_dark,
            ),
          ),
        );
        break;
      case "settings":
        return InkWell(
          highlightColor: Colors.transparent,
          onTap: () {
            RM.get<CoursesService>().setState(
                  (state) => state.options = !state.options,
                );
          },
          child: CircleAvatar(
            backgroundColor: Colors.white,
            radius: 20,
            child: Icon(
              Icons.settings,
              color: bg_dark,
            ),
          ),
        );
        break;
      case "search":
        return InkWell(
          highlightColor: Colors.transparent,
          onTap: () {
            RM.get<CoursesService>().setState(
                  (state) => state.search = !state.search,
                );
          },
          child: CircleAvatar(
            backgroundColor: Colors.white,
            radius: 20,
            child: Icon(
              Icons.search,
              color: bg_dark,
            ),
          ),
        );
        break;
      case "close":
        return InkWell(
          highlightColor: Colors.transparent,
          onTap: () {
            Navigator.pop(context);
          },
          child: CircleAvatar(
            backgroundColor: bg_dark,
            radius: 20,
            child: Icon(
              Icons.clear,
              color: white,
            ),
          ),
        );
        break;
      default:
        return SizedBox(width: 40);
        break;
    }
  }
}
