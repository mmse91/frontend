import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PhotoLayout extends StatelessWidget {
  final image;
  final bool card;

  PhotoLayout(this.image, {this.card = true});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: openPhotoDialog(context),
        child: card
            ? Container(
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.1),
                        spreadRadius: 3,
                        blurRadius: 5,
                        offset: Offset(0, 5), // changes position of shadow
                      ),
                    ]),
                child: Image(image: image))
            : Image(image: image));
  }

  VoidCallback openPhotoDialog(BuildContext context) => () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
              child: Container(
                child: PhotoView(
                  backgroundDecoration: BoxDecoration(color: Colors.white),
                  tightMode: true,
                  imageProvider: image,
                  maxScale: PhotoViewComputedScale.covered * 2.0,
                  minScale: PhotoViewComputedScale.contained,
                  initialScale: PhotoViewComputedScale.contained,
                ),
              ),
            );
          },
        );
      };
}
