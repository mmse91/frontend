import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:flutter/material.dart';

class FbsListCard extends StatelessWidget {
  FbsListCard(
      {this.height,
      this.heading,
      this.subHeading,
      this.icon,
      this.customIconText,
      this.optionalSubHeading});

  final double height;
  final String heading;
  final String subHeading;
  final String optionalSubHeading;
  final Icon icon;
  final String customIconText;

  String getHeading() {
    if (this.heading == null) return "Kein Text vorhanden";
    return this.heading.trim();
  }

  String getSubHeading() {
    if (this.subHeading == null) return "Kein Text vorhanden";
    return this.subHeading.trim();
  }

  String getOptionalSubHeading() {
    if (this.optionalSubHeading == "") return "Kein Text vorhanden";
    return this.optionalSubHeading.trim();
  }

  double getHeight(BuildContext context) {
    if (height == null)
      return optionalSubHeading != null ? (((MediaQuery.of(context).size.width) >  768) ? 110 : 85) : (((MediaQuery.of(context).size.width) >  768) ? 95 : 70);
    else
      return (((MediaQuery.of(context).size.width) >  768) ? height*1.5 : height);
  }

  @override
  Widget build(BuildContext context) {
    return InkResponse(
      child: Container(
          height: getHeight(context),
          margin: EdgeInsets.symmetric(vertical: 6),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.65 > 500
                          ? 650 * 0.65
                          : MediaQuery.of(context).size.width * 0.65),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        getHeading(),
                        style: ((MediaQuery.of(context).size.width) < 768) ? headline5 : headline5Web,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            optionalSubHeading != null
                                ? Text(
                                    getOptionalSubHeading(),
                                    style: ((MediaQuery.of(context).size.width) < 768) ? headline6 : headline6Web,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  )
                                : Container(),
                            Text(
                              getSubHeading(),
                              maxLines: 1,
                              style: ((MediaQuery.of(context).size.width) < 768) ? headline6 : headline6Web,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ]),
                    ],
                  ),
                ),
                icon != null
                    ? icon
                    : customIconText != null
                        ? CircleAvatar(
                            backgroundColor: bg_dark,
                            radius: 15,
                            child: Text(customIconText),
                          )
                        : Container(),
              ],
            ),
          )),
    );
  }
}
