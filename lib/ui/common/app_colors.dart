import 'package:flutter/material.dart';

const Color bg_light = Color.fromARGB(255, 242, 245, 246);
const Color bg_green = Color.fromARGB(255, 223, 238, 200);
const Color bg_dark = Color.fromARGB(255, 74, 92, 102);

const Color green = Color.fromARGB(255, 128, 186, 36);
const Color yellow = Color.fromARGB(255, 249, 212, 127);
const Color blue = Color.fromARGB(255, 64, 94, 154);
const Color red = Color.fromARGB(255, 181, 78, 98);
const Color white = Color.fromARGB(255, 255, 255, 255);

const Color font_dark = Color.fromARGB(255, 57, 74, 89);
const Color font_red = Color.fromARGB(255, 156, 19, 46);

const Color input_grey = Color.fromARGB(255, 150, 150, 150);
