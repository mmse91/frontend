import 'package:flutter/widgets.dart';
import 'index.dart';

abstract class QuestionStatistic {
  covariant Question question;
  int answered;
  String type;

  static QuestionStatistic fromJson(Map<String, dynamic> json) {
    final type = json['type'];
    switch (type) {
      case 'multipleChoice':
        return MultipleChoiceStatistic.fromJson(json);
      case 'open':
        return OpenStatistic.fromJson(json);
      case 'sort':
        return SortStatistic.fromJson(json);
      /*case 'pairs':
        return PairsQuestion.fromJson(json);*/ // <- Unimplemented in Frontend
      case 'wordCloud':
        return WordCloudStatistic.fromJson(json);
      case 'rating':
        return RatingStatistic.fromJson(json);
      default:
        throw UnimplementedError(
            'Questions with type $type are yet unimplemented.'); // <- Programmierfehler
    }
  }
}

class MultipleChoiceStatistic implements QuestionStatistic {
  MultipleChoiceQuestion question;
  String type = "Multiple Choice";
  int answered;
  List<int> _stats;

  get stats => _stats.any((element) => element > 0) ? _dataMap() : {};

  MultipleChoiceStatistic.fromJson(Map<String, dynamic> json) {
    this.question = MultipleChoiceQuestion.fromJson(json);
    this.answered = json['answered'];
    this._stats = List<int>.from(json['stats']);
  }

  Map<String, double> _dataMap() {
    return {
      (question.answers[0].correct
          ? '\u{2713} ${question.answers[0].answer}'
          : '     ${question.answers[0].answer}'): _stats[0].toDouble(),
      (question.answers[1].correct
          ? '\u{2713} ${question.answers[1].answer}'
          : '     ${question.answers[1].answer}'): _stats[1].toDouble(),
      (question.answers[2].correct
          ? '\u{2713} ${question.answers[2].answer}'
          : '     ${question.answers[2].answer}'): _stats[2].toDouble(),
      (question.answers[3].correct
          ? '\u{2713} ${question.answers[3].answer}'
          : '     ${question.answers[3].answer}'): _stats[3].toDouble(),
    };
  }

  @override
  String toString() {
    return 'MCStat Question: $question, answered: $answered, stats: $stats';
  }
}

class OpenStatistic implements QuestionStatistic {
  OpenQuestion question;
  String type = "Freitext";
  int answered;
  List<String> stats;

  OpenStatistic.fromJson(Map<String, dynamic> json) {
    this.question = OpenQuestion.fromJson(json);
    this.answered = json['answered'];
    this.stats = List<String>.from(json['stats']);
  }

  @override
  String toString() {
    return 'OpenStat Question: $question, answered: $answered, stats: $stats';
  }
}

class SortStatistic implements QuestionStatistic {
  SortQuestion question;
  String type = "Reihenfolge";
  int answered;
  int _correct;
  int _incorrect;

  get stats => _correct > 0 || _incorrect > 0 ? _dataMap() : {};

  SortStatistic.fromJson(Map<String, dynamic> json) {
    this.question = SortQuestion.fromJson(json);
    this.answered = json['answered'];
    this._correct = json['stats']['correct'];
    this._incorrect = json['stats']['incorrect'];
  }

  Map<String, double> _dataMap() {
    return {
      "Richtig sortiert": _correct.toDouble(),
      "Falsch sortiert": _incorrect.toDouble()
    };
  }

  @override
  String toString() {
    return 'SortStat Question: $question, answered: $answered, stats: correct $_correct, incorrect $_incorrect';
  }
}

class WordCloudStatistic implements QuestionStatistic {
  WordCloudQuestion question;
  String type = "Wordcloud";
  int answered;
  Map<String, int> stats;
  MemoryImage image;

  WordCloudStatistic.fromJson(Map<String, dynamic> json) {
    this.question = WordCloudQuestion.fromJson(json);
    this.answered = json['answered'];
    this.stats = Map<String, int>.from(json['stats']);
  }

  @override
  String toString() {
    return 'WordcloudStat Question: $question, answered: $answered, stats: $stats';
  }
}

class RatingStatistic implements QuestionStatistic {
  RatingQuestion question;
  String type = "Bewertungsskala";
  int answered;
  List<int> _stats;

  Map<String, double> get stats =>
      _stats.any((element) => element > 0) ? _dataMap() : {};

  double get mean => _mean();

  RatingStatistic.fromJson(Map<String, dynamic> json) {
    this.question = RatingQuestion.fromJson(json);
    this.answered = json['answered'];
    this._stats = List<int>.from(json['stats']);
  }

  Map<String, double> _dataMap() {
    var data = new Map<String, double>();
    data = {
      "1 - Trifft voll zu": _stats[0].toDouble(),
      "2 - Trifft zu": _stats[1].toDouble(),
      "3 - Neutral": _stats[2].toDouble(),
      "4 - Trifft nicht zu": _stats[3].toDouble(),
      "5 - Trifft überhaupt nicht zu": _stats[4].toDouble(),
    };
    return data;
  }

  double _mean() {
    var mean = 0;
    for (var i = 0; i < _stats.length; i++) {
      mean = mean + (_stats[i] * (i + 1));
    }
    return mean / answered.toDouble();
  }

  @override
  String toString() {
    return 'RatingStat Question: $question, stats: $_stats';
  }
}
