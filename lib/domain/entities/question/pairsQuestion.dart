import 'question.dart';
import 'answer.dart';

//Not implemented in Frontend
class PairsQuestion implements Question {
  int number;
  String question;
  List<PairsAnswer> answers;

  PairsQuestion({this.number, this.question, this.answers});

  PairsQuestion.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    question = json['question']['question'];
    if (json['question']['answers'] != null) {
      answers = answersFromJson(json['question']['answers']);
    }
  }

  List<PairsAnswer> answersFromJson(List<dynamic> json) {
    return json.map((e) => PairsAnswer.fromJson(e)).toList();
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final Map<String, dynamic> questionData = new Map<String, dynamic>();
    data['type'] = 'pairs';
    if (number != null) {
      data['number'] = this.number;
    }
    questionData['question'] = this.question;
    questionData['answers'] = this.answers.map((e) => e.toJson()).toList();
    data['question'] = questionData;

    return data;
  }

  @override
  String toString() {
    return 'Pairs Question number: $number, question: $question, answers: $answers';
  }
}

class PairsAnswer implements Answer {
  List<String> answer;

  PairsAnswer(List<String> answer) : this.answer = answer;

  PairsAnswer.fromJson(List<dynamic> json) {
    answer = json.map((e) => (e).toString()).toList();
  }

  List<String> toJson() {
    return answer;
  }

  @override
  String toString() {
    return 'Answers: $answer';
  }
}
