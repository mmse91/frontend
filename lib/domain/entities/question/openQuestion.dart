import 'answer.dart';
import 'question.dart';

class OpenQuestion implements Question {
  int number;
  String question;
  List<OpenAnswer> answers = [new OpenAnswer("")];

  OpenQuestion({this.number, this.question});

  OpenQuestion.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    question = json['question']['question'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final Map<String, dynamic> questionData = new Map<String, dynamic>();
    data['type'] = 'open';
    if (number != null) {
      data['number'] = this.number;
    }
    questionData['question'] = this.question;
    data['question'] = questionData;
    return data;
  }

  @override
  String toString() {
    return 'Open Question number: $number, question: $question';
  }
}

class OpenAnswer implements Answer {
  String answer;

  OpenAnswer(String answer) : this.answer = answer;

  String toJson() {
    return answer;
  }

  @override
  String toString() {
    return 'Answer: $answer';
  }
}
