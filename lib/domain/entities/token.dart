/// Represents a session or authorization token in [token].
class Token {
  final String token;

  Token(this.token);

  /// Returns a [Token] object from the given [json].
  Token.fromJson(Map<String, dynamic> json) : token = json['token'];

  @override
  String toString() {
    return token;
  }
}
