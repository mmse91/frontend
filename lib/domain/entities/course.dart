import 'package:fbs_app/domain/entities/user.dart';

class Course {
  int id;
  String name;
  String description;
  int password;
  String semester;
  bool protected;
  String type;
  User docent;

  //Typically called form service layer to create a new course
  Course({
    this.id,
    this.name,
    this.description,
    this.password,
    this.semester,
    this.protected,
    this.type,
    this.docent,
  });

  //Typically called from data_source layer after getting data from external source.
  Course.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    semester = getSemester(json['semester']);
    protected = json['protected'];
    if (json['docent'] != null) {
      docent = User.fromJson(json['docent']);
    }
  }

  //For admin flag
  Course.fromJsonForUser(Map<String, dynamic> json) {
    var course = json['course'];
    id = course['id'];
    name = course['name'];
    description = course['description'];
    semester = getSemester(course['semester']);
    protected = course['protected'];
    if (course['docent'] != null) {
      docent = User.fromJson(course['docent']);
    }
    type = json['type'];
  }

  //Typically called from service or data_source layer just before persisting data.
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    if (password != null) {
      data['password'] = this.password;
    }
    data['semester'] = this.semester;
    data['protected'] = this.protected;
    return data;
  }

  @override
  String toString() {
    return "Course id: $id, name: $name, description: $description, passwort: $password, semester: $semester, docent: $docent, type: $type";
  }

  static String getSemester(String semester) {
    if (semester.length != 8) return "Semester unbekannt";
    String semesterType = semester.substring(0, 4);
    String semesterYear = semester.substring(4);
    if (semesterType == 'wise') {
      String year = (int.parse(semesterYear.substring(2)) + 1).toString();
      semesterYear = semesterYear + '/' + year;
    }
    semesterType = semesterType == 'sose' ? 'Sommersemester' : 'Wintersemester';
    return semesterType + ' ' + semesterYear;
  }
}
