class InvalidCredentialsException implements Exception {
  String message;

  InvalidCredentialsException.msg(String msg) : this.message = msg;

  InvalidCredentialsException() : this.message = 'Ungültige Anmeldedaten.';

  String toString() {
    return "InvalidCredentialsException: $message";
  }
}
