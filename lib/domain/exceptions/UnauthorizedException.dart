class UnauthorizedException implements Exception {
  String message;

  UnauthorizedException()
      : this.message =
            'Es fehlen notwendigen Berechtigungen für diese Anfrage.';

  UnauthorizedException.msg(String msg) : this.message = msg;

  String toString() {
    return "UnauthorizedException: $message";
  }
}
