class NetworkErrorException implements Exception {
  String message;

  NetworkErrorException.msg(String msg) : this.message = msg;

  NetworkErrorException()
      : this.message = 'Die Anfrage konnte nicht ordentlich bearbeitet werden.';

  String toString() {
    return "NetworkErrorException: $message";
  }
}
